var tp = '';
var map;
var geo;
var marker;
jQuery(document).ready(function() {
  if(jQuery("#marker_map").length) {
    
    setTimeout(function() { getToken(); }, 1000);

	/**
	 * On change of latlng value
	 */
	jQuery("#latitude, #longitude").on("input", function(){
	  if(jQuery("#latitude").val().trim()!="" && jQuery("#longitude").val().trim() != ""  ){
		marker.setLngLat([jQuery("#longitude").val(), jQuery("#latitude").val()]).addTo(map);
		map.setCenter([jQuery("#longitude").val(), jQuery("#latitude").val()]);
	  }
	});

    /**
	 * On change of `map`, set `map` style
	 */
	jQuery("#map_id").on("input", function(){
	  let styleV = (jQuery("#map_id").val().trim() != "" ? jQuery("#map_id option:selected").attr('data-map_style') : "mapbox://styles/mapbox/streets-v11");
	  map.setStyle(styleV);
	});
	
	/**
	 * Change marker on change of style
	 */
	jQuery("#marker_style").on("input", function(){
	  let lat = ( (jQuery("#latitude").val().trim() != "" && jQuery("#latitude").val() != 0) ? jQuery("#latitude").val().trim() : 0.1),
        lng = ( (jQuery("#longitude").val().trim() != "" && jQuery("#longitude").val() != 0) ? jQuery("#longitude").val().trim() : 0.1),
		color = jQuery("#markerColor").val();

	  marker.remove();
	  var markerEle = document.createElement('div');
	  markerEle.className = 'treweler-marker';
	  if(jQuery(this).val() == "light") {
	    markerEle.innerHTML = '<div class="marker"><div class="marker-wrap"><div class="marker__shadow"><div class="marker__border" style="border-color:'+ color +';"><div class="marker__center"></div></div></div></div></div>';
	  } else if(jQuery(this).val() == "dark") {
	    markerEle.innerHTML = '<div class="marker marker--dark"><div class="marker-wrap"><div class="marker__shadow"><div class="marker__border" style="border-color:'+ color +';"><div class="marker__center"></div></div></div></div></div>';
	  }
	  marker = new mapboxgl.Marker(markerEle, {draggable: true}).setLngLat([lng, lat]).addTo(map);

	  marker.on('dragend', function(e) {
		let ll = marker.getLngLat();
		jQuery("#latitude").val(ll.lat);
		jQuery("#longitude").val(ll.lng);
	  });
	});

	/**
	 * Fetch `icon` select event from WP Media
	 */
	if (wp.media) {
		var featuredImageObj = wp.media.featuredImage.frame();
		featuredImageObj.on('select', function(e) {
			let selectedImg = featuredImageObj.state().get('selection').first().toJSON(); /* selectedImg.url */
			let lat = ( (jQuery("#latitude").val().trim() != "" && jQuery("#latitude").val() != 0) ? jQuery("#latitude").val().trim() : 0.1),
				lng = ( (jQuery("#longitude").val().trim() != "" && jQuery("#longitude").val() != 0) ? jQuery("#longitude").val().trim() : 0.1),
				anchorV = jQuery("#marker_position").val();
			if(selectedImg.subtype == "svg+xml") {
				
				jQuery.ajax({
					type: "GET",
					url: selectedImg.url,
					success: function(data){
						let svgEle = document.createElement("div");
						svgEle.innerHTML = new XMLSerializer().serializeToString(data.documentElement);
						let svgH = parseInt(jQuery(svgEle).find("svg").attr("height")),
							svgW = parseInt(jQuery(svgEle).find("svg").attr("width"));

						let iHeight = svgH <= 42 ? (svgH%2==0?svgH:svgH+1) : 42,
							iWidth = svgW <= 42 ? (svgW%2==0?svgW:svgW+1) : 42;

						/* remove marker */
						marker.remove();
						/* add icon */
						var markerEle = document.createElement('div');
						markerEle.className = 'treweler-marker icon';
						markerEle.style.backgroundImage = 'url('+selectedImg.url+')';
						markerEle.style.backgroundSize = 'contain';
						markerEle.style.backgroundRepeat = 'no-repeat';
						markerEle.style.backgroundPosition = 'center top';
						markerEle.style.width = iWidth+'px';
						markerEle.style.height = iHeight+'px';

						marker = new mapboxgl.Marker(markerEle, {draggable: true, anchor: anchorV}).setLngLat([lng, lat]).addTo(map);
						/**
						 * Change marker position on drag
						 */
						marker.on('dragend', function(e) {
						  let ll = marker.getLngLat();
						  jQuery("#latitude").val(ll.lat);
						  jQuery("#longitude").val(ll.lng);
						});

						jQuery("#marker_icon_size").val(iHeight+";"+iWidth);
						
					}
				})

			} else {

				let iHeight = selectedImg.height <= 42 ? (selectedImg.height%2==0 ? selectedImg.height : selectedImg.height+1) : 42,
					iWidth = selectedImg.width <= 42 ? (selectedImg.width%2==0 ? selectedImg.width : selectedImg.width+1) : 42;

				/* remove marker */
				marker.remove();
				/* add icon */
				var markerEle = document.createElement('div');
				markerEle.className = 'treweler-marker icon';
				markerEle.style.backgroundImage = 'url('+selectedImg.url+')';
				markerEle.style.backgroundSize = 'contain';
				markerEle.style.backgroundRepeat = 'no-repeat';
				markerEle.style.backgroundPosition = 'center top';
				markerEle.style.width = iWidth+'px';
				markerEle.style.height = iHeight+'px';

				marker = new mapboxgl.Marker(markerEle, {draggable: true, anchor: anchorV}).setLngLat([lng, lat]).addTo(map);
				/**
				 * Change marker position on drag
				 */
				marker.on('dragend', function(e) {
				  let ll = marker.getLngLat();
				  jQuery("#latitude").val(ll.lat);
				  jQuery("#longitude").val(ll.lng);
				});

				jQuery("#marker_icon_size").val(iHeight+";"+iWidth);
			}

		});
	}
	jQuery("#postimagediv").on("click", "#remove-post-thumbnail", function() {
		addHTMLMarker();
		jQuery("#marker_icon_size").val("");
	});

  }

  jQuery("#color-picker-btn, .clr-picker span").on("click", function() {
	if(jQuery(".color-picker").find(".a-color-picker").length === 0) {
	    AColorPicker.from('.color-picker')
		.on('change', (picker, color) => {
		  jQuery(".treweler-marker .marker .marker__border").css("border-color", picker.color);
		  jQuery(".clr-picker span").css("background-color", picker.color);
		  jQuery("#markerColor").val(picker.color);
		  jQuery(".color-picker").attr('acp-color', picker.color);
		})
		.on('coloradd', (picker, color) => {
		  let cca = jQuery("#addCustomColor");
		  if(cca.val().indexOf("|"+color) === -1) {
		    cca.val(cca.val()+"|"+color); 
		    jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: cca.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		}) 
		.on('colorremove', (picker, color) => {
		  let ccr = jQuery("#addCustomColor");
		  if(ccr.val().indexOf("|"+color) != -1) {
			let sc = ccr.val().replace("|"+color, "");
			ccr.val(sc);
			jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: ccr.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		});

	} else {
	  jQuery(".color-picker").find(".a-color-picker").remove();
	}
  });

  jQuery(window).on("click", function(event) { 
	if(!jQuery(event.target).hasClass("a-color-picker") && jQuery(event.target).parents(".a-color-picker").length == 0 && jQuery(event.target).attr('id') != 'color-picker-btn' && !jQuery(event.target).hasClass("color-holder") && !jQuery(event.target).hasClass("a-color-picker-palette-color")) {
	  jQuery(".color-picker").find(".a-color-picker").remove();
	}
  });

});

jQuery(document).on("change", "#marker_position", function(){
  let imgObj = jQuery("#set-post-thumbnail img");
  if(imgObj.length != undefined && imgObj.length > 0) {

	let lat = ( (jQuery("#latitude").val().trim() != "" && jQuery("#latitude").val() != 0) ? jQuery("#latitude").val().trim() : 0.1),
		lng = ( (jQuery("#longitude").val().trim() != "" && jQuery("#longitude").val() != 0) ? jQuery("#longitude").val().trim() : 0.1),
		imgURL = jQuery("#set-post-thumbnail img").attr("src"),
		anchorV = jQuery(this).val(),
		imgSize = jQuery("#marker_icon_size").val() != "" ? jQuery("#marker_icon_size").val().split(";") : "42;42".split(";");

	let	iHeight = parseInt(imgSize[0]) <= 42 ? (parseInt(imgSize[0])%2==0 ? parseInt(imgSize[0]) : parseInt(imgSize[0])+1) : 42;
		iWidth = parseInt(imgSize[1]) <= 42 ? (parseInt(imgSize[1])%2==0 ? parseInt(imgSize[1]) : parseInt(imgSize[1])+1) : 42;

		/* remove marker */
		marker.remove();

		/* Add Marker */
		var markerEle = document.createElement('div');
		markerEle.className = 'treweler-marker icon';
		markerEle.style.backgroundImage = 'url('+imgURL+')';
		markerEle.style.backgroundSize = 'contain';
		markerEle.style.backgroundRepeat = 'no-repeat';
		markerEle.style.backgroundPosition = 'center top';
		markerEle.style.width = parseInt(iWidth)+'px';
		markerEle.style.height = parseInt(iHeight)+'px';
	/*	markerEle.style.imageRendering = '-webkit-optimize-contrast'; */
		marker = new mapboxgl.Marker(markerEle, {draggable: true, anchor: anchorV}).setLngLat([lng, lat]).addTo(map);

	    /**
		 * Change marker position on drag
		 */
		marker.on('dragend', function(e) {
		  let ll = marker.getLngLat();
		  jQuery("#latitude").val(ll.lat);
		  jQuery("#longitude").val(ll.lng);
		});

  }
});

/**
 * Get mapbox token 
 */
function getToken() {
  if(tp.length == 0) {
    jQuery.ajax({
      url: obj.ajax_url,
      type : 'GET',
      data : { action : 'treweler_get_admin_token' },
      success: function( response ) {
        res = atob( response ).replace("###", "");
        tp = res.replace("###", "");
		init_map();
      }
    });
  }
}

/**
 * Initialize map
 */
function init_map() {
  jQuery("#marker_map").html("");
  if(tp.length != 0) {

    let lat = ( (jQuery("#latitude").val().trim() != "" && jQuery("#latitude").val() != 0) ? jQuery("#latitude").val().trim() : 0.1),
        lng = ( (jQuery("#longitude").val().trim() != "" && jQuery("#longitude").val() != 0) ? jQuery("#longitude").val().trim() : 0.1),
        zoom = (jQuery("#setZoom").val().trim() != "" ? jQuery("#setZoom").val() : 0),
		map_style = (jQuery("#map_id").val().trim() != "" ? jQuery("#map_id option:selected").attr('data-map_style') : "mapbox://styles/mapbox/streets-v11"),
		color = jQuery("#markerColor").val();

    mapboxgl.accessToken = tp;
    map = new mapboxgl.Map({
      container: 'marker_map',
      style: map_style,
      center: [lng, lat],
      zoom: zoom,
	  minZoom: 0,
	  maxZoom: 24
    });

	if(!jQuery("#set-post-thumbnail img").length) {

	  var markerEle = document.createElement('div');
	  markerEle.className = 'treweler-marker';
	  if(jQuery("#marker_style").val() == "light") {
	    markerEle.innerHTML = '<div class="marker"><div class="marker-wrap"><div class="marker__shadow"><div class="marker__border" style="border-color:'+color +';"><div class="marker__center"></div></div></div></div></div>';
	  } else if(jQuery("#marker_style").val() == "dark") {
	    markerEle.innerHTML = '<div class="marker marker--dark"><div class="marker-wrap"><div class="marker__shadow"><div class="marker__border" style="border-color:'+color +';"><div class="marker__center"></div></div></div></div></div>';
	  }
	  marker = new mapboxgl.Marker(markerEle, {draggable: true}).setLngLat([lng, lat]).addTo(map);

	} else {

	  let imgURL = jQuery("#set-post-thumbnail img").attr("src"),
		  anchorV = jQuery("#marker_position").val(),
		  imgSize = jQuery("#marker_icon_size").val() != "" ? jQuery("#marker_icon_size").val().split(";") : "42;42".split(";");

	  let iHeight = parseInt(imgSize[0]) <= 42 ? (parseInt(imgSize[0])%2==0 ? parseInt(imgSize[0]) : parseInt(imgSize[0])+1) : 42;
		  iWidth = parseInt(imgSize[1]) <= 42 ? (parseInt(imgSize[1])%2==0 ? parseInt(imgSize[1]) : parseInt(imgSize[1])+1) : 42;

	  var markerEle = document.createElement('div');
		  markerEle.className = 'treweler-marker icon';
		  markerEle.style.backgroundImage = 'url('+ imgURL +')';
		  markerEle.style.backgroundSize = 'contain';
		  markerEle.style.backgroundRepeat = 'no-repeat';
		  markerEle.style.backgroundPosition = 'center top';
		  markerEle.style.width = parseInt(iWidth)+'px';
		  markerEle.style.height = parseInt(iHeight)+'px';
		  marker = new mapboxgl.Marker(markerEle, {draggable: true, anchor: anchorV}).setLngLat([lng, lat]).addTo(map);

	}

	/**
	 * Change marker position on drag
	 */
	marker.on('dragend', function(e) {
	  let ll = marker.getLngLat();
	  jQuery("#latitude").val(ll.lat);
	  jQuery("#longitude").val(ll.lng);
	});

	/**
	 * Change marker position on click
	 */
	map.on('click', function(e) {
	  marker.setLngLat([e.lngLat.lng, e.lngLat.lat]).addTo(map);
	  jQuery("#latitude").val(e.lngLat.lat);
	  jQuery("#longitude").val(e.lngLat.lng);
	});

	/**
	 * Change zoom value
	 */
	map.on('zoom', function(e) {
	  let zoomV = map.getZoom().toFixed(2);
	  jQuery("#setZoom").val(zoomV);
	});

  } else {
	jQuery("#marker_map").append('<p class="notice notice-error settings-error is-dismissible">Mapbox access token is required.</p>');
  }
}

/**
 * Add HTML Marker
 */
function addHTMLMarker() {
	let lat = ( (jQuery("#latitude").val().trim() != "" && jQuery("#latitude").val() != 0) ? jQuery("#latitude").val().trim() : 0.1),
        lng = ( (jQuery("#longitude").val().trim() != "" && jQuery("#longitude").val() != 0) ? jQuery("#longitude").val().trim() : 0.1),
		color = jQuery("#markerColor").val();

	/* remove marker */
	marker.remove();

	/* Add Marker */
	var markerEle = document.createElement('div');
	markerEle.className = 'treweler-marker';
	if(jQuery("#marker_style").val() == "light") {
		markerEle.innerHTML = '<div class="marker"><div class="marker-wrap"><div class="marker__shadow"><div class="marker__border" style="border-color:'+ color +';"><div class="marker__center"></div></div></div></div></div>';
	} else if(jQuery("#marker_style").val() == "dark") {
		markerEle.innerHTML = '<div class="marker marker--dark"><div class="marker-wrap"><div class="marker__shadow"><div class="marker__border" style="border-color:'+ color +';"><div class="marker__center"></div></div></div></div></div>';
	}
	marker = new mapboxgl.Marker(markerEle, {draggable: true}).setLngLat([lng, lat]).addTo(map);

	/**
	 * Change marker position on drag
	 */
	marker.on('dragend', function(e) {
	  let ll = marker.getLngLat();
	  jQuery("#latitude").val(ll.lat);
	  jQuery("#longitude").val(ll.lng);
	});
}