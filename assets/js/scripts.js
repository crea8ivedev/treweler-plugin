var tp = '';
var map;
var geo;

jQuery(document).ready(function() {
  if(jQuery("#map").length) {
    
    setTimeout(function() { getToken(); }, 1000);
	/* Controls */
    jQuery('input[type="checkbox"]').on('change', function() {
      /* Fullscreen control */
	  if( jQuery(this).val() === "Fullscreen" ) {
        var fs = new mapboxgl.FullscreenControl();
		if( jQuery(this).prop("checked") ) {
          map.addControl(fs);
		  jQuery(map._container).find(".mapboxgl-ctrl-fullscreen").parent(".mapboxgl-ctrl-group").addClass('treweler-fs-ctrl');
        } else {
		  jQuery(map._container).find(".mapboxgl-ctrl-fullscreen").parent(".mapboxgl-ctrl-group").remove();
        }
      }
	  /* Zoom & Pan Control */
	  if( jQuery(this).val() === "Zoom & Pan" ) {
        var zp = new mapboxgl.NavigationControl();
		if( jQuery(this).prop("checked") ) {
          map.addControl(zp);
		  jQuery(map._container).find(".mapboxgl-ctrl-zoom-in").parent(".mapboxgl-ctrl-group").addClass('treweler-zp-ctrl');
        } else {
		  jQuery(map._container).find(".mapboxgl-ctrl-zoom-in").parent(".mapboxgl-ctrl-group").remove();
        }
      }
	  /* Distance Scale Control */
	  if( jQuery(this).val() === "Distance scale" ) {
        var ds = new mapboxgl.ScaleControl( { maxWidth: 100, unit: 'imperial' } );
		if( jQuery(this).prop("checked") ) {
          map.addControl(ds, 'bottom-right');
        } else {
		  jQuery(map._container).find(".mapboxgl-ctrl-scale").remove();
        }
      }
	  /* Search Control */
	  if( jQuery(this).val() === "Search" ) {
		if( jQuery(this).prop("checked") ) {
			map.addControl(
              geo = new MapboxGeocoder({
                accessToken: mapboxgl.accessToken,
                zoom: 14,
                placeholder: 'Enter search...',
                mapboxgl: mapboxgl,
		        marker: false
              }), "top-left"
            );
			geo.on('result', function(res){
			  let latlng = res.result.center;
			  jQuery("#latitude").val(latlng[1]);
			  jQuery("#longitude").val(latlng[0]);
			});
		} else {
			jQuery(map._container).find(".mapboxgl-ctrl-geocoder").remove();
		}
      }
    });

	/**
     * Change center point 
     */
    jQuery('#map_styles, #custom_style').on('change keyup', function() {
	  styleType = (jQuery("#custom_style").val().trim() != "" ? jQuery("#custom_style").val() : jQuery("#map_styles").val());
      map.setStyle(styleType);
    });

    /**
	 * Enable Map Pan If "Use the map preview" is checked
	 */
    jQuery('input[type="checkbox"]#latlng_map_prev, input[type="checkbox"]#zoom_map_prev').on('change', function() {
      /* Map View Point Control Preview */
      if( jQuery(this).attr('id') == 'latlng_map_prev' ) {
        if( jQuery(this).prop("checked") ) {
          jQuery("#latitude").attr('readonly', 'readonly');
          jQuery("#longitude").attr('readonly', 'readonly');

		  /* Enable Map Drag */
		  map.dragPan.enable();
		  map.dragRotate.enable();
		  map.on("drag", function(){
			let lnglatC = map.getCenter();
			jQuery("#longitude").val(lnglatC.lng);
			jQuery("#latitude").val(lnglatC.lat);
		  });
        } else {
          jQuery("#latitude").removeAttr('readonly');
          jQuery("#longitude").removeAttr('readonly');

		  /* Disable Map Drag */
		  map.dragPan.disable();
		  map.dragRotate.disable();
		}
      }

      /* Map Zoom Control Preview */
      if( jQuery(this).attr('id') == "zoom_map_prev" ) {
        if( jQuery(this).prop("checked") ) {
          jQuery("#setZoom_range").attr('disabled', 'disabled');
          jQuery("#setZoom").attr('readonly', 'readonly');

		  /* Enable Map Zoom on click/scroll */
		  map.doubleClickZoom.enable();
		  map.scrollZoom.enable();
		  map.dragRotate.enable();
        } else {
		  jQuery("#setZoom_range").removeAttr('disabled');
          jQuery("#setZoom").removeAttr('readonly');

		  /* Disable Map Zoom on click/scroll */
		  map.doubleClickZoom.disable();
		  map.scrollZoom.disable();
		  map.dragRotate.disable();
		}
      }

	  /* Map Latlng prev checkbox unchecked & zoom prev checked */
	  if( !jQuery("#latlng_map_prev").prop("checked") && jQuery("#zoom_map_prev").prop("checked") ) {
		map.on("wheel", function(e){
		  if(!jQuery("#latlng_map_prev").prop("checked") && jQuery("#zoom_map_prev").prop("checked")) {
			map.scrollZoom.setWheelZoomRate(1);
		    let mZoom = parseFloat(map.getZoom()).toFixed(2),
		  	latW = parseFloat(jQuery("#latitude").val()),
		  	lngW = parseFloat(jQuery("#longitude").val());
		  	map.zoomTo(mZoom, {center:[lngW, latW]});
		  }
		});
		map.on("dblclick", function(e){
		  if(!jQuery("#latlng_map_prev").prop("checked") && jQuery("#zoom_map_prev").prop("checked")) {
		    e.preventDefault();
		    let dcZoom = parseInt(map.getZoom()),
		      latDC = parseFloat(jQuery("#latitude").val()),
		      lngDC = parseFloat(jQuery("#longitude").val());
		      jQuery("#setZoom").val(dcZoom);
		      jQuery("#setZoom_range").val(dcZoom);
		      e.lngLat.lng = lngDC;
		      e.lngLat.lat = latDC;
		      e._defaultPrevented = false;
		      map.doubleClickZoom.enable();
		      map.zoomTo(dcZoom, {center:[lngDC, latDC]});
		  }
		});
	  }
    });

    /**
     * Change zoom level based on control value 
     */
    jQuery('#latitude, #longitude').on('change keyup', function() {
      if(jQuery("#latitude").val().trim() != "" && jQuery("#longitude").val().trim() != ""){
        map.setCenter([jQuery("#longitude").val(), jQuery("#latitude").val()]);
      }
    });

    /**
     * Change zoom level based on control value 
     */
    jQuery('#setZoom, #setZoom_range').on('input', function() {
      let zoomCC = parseFloat(jQuery(this).val()).toFixed(2),
          latN = parseFloat(jQuery("#latitude").val()),
          lngN = parseFloat(jQuery("#longitude").val());
		  if(zoomCC.trim() == "" || zoomCC.trim() == "NaN" || zoomCC == 0.00 || zoomCC == 0.23){
			zoomCC = 0;
		  }
          map.zoomTo(zoomCC, { center:[lngN, latN], duration: 2000 });
		  if(jQuery(this).attr('id') == "setZoom") {
			  jQuery('#setZoom_range').val(zoomCC);
		  } else {
			  jQuery('#setZoom').val(zoomCC);
		  }
    });

    /**
     * Set zoom min/max level 
     */
    jQuery('#setZoom_min_range, #setZoom_max_range').on('input', function() { 
      lR = parseFloat(jQuery('#setZoom_min_range').val());
      uR = parseFloat(jQuery('#setZoom_max_range').val());

	  if(jQuery(this).hasClass("lower-slider")) {
		if(!jQuery(this).hasClass("active")){
		  jQuery(this).addClass("active");
		  jQuery(".upper-slider").removeClass("active");
		}
		if(uR <= lR+1) {
		  jQuery('#setZoom_max_range').val(lR+2);
		  if(uR == lR){
			jQuery('#setZoom_max_range').val(lR+2);
		  }
		}
	  } else if(jQuery(this).hasClass("upper-slider")) {
		if(!jQuery(this).hasClass("active")){
		  jQuery(this).addClass("active");
		  jQuery(".lower-slider").removeClass("active");
		}
		if (lR >= uR - 1) {
		  jQuery('#setZoom_min_range').val(uR-2);
		  if(uR == lR){
			jQuery('#setZoom_min_range').val(uR-2);
		  }
		}
	  }
	  let min = parseFloat(jQuery('#setZoom_min_range').val()), max = parseFloat(jQuery('#setZoom_max_range').val());

	  jQuery("#default_min_zoom").val(min);
	  jQuery("#default_max_zoom").val(max);
	  jQuery("#setZoom_range").attr({"min": min, "max": max});
	  jQuery("#setZoom").attr({"min": min, "max": max});

	  if(min >= parseFloat(jQuery("#setZoom").val())) {
		  jQuery("#setZoom").val(min);
		  jQuery("#setZoom_range").val(min).trigger('input');
	  } else if(max <= parseFloat(jQuery("#setZoom").val())){
		  jQuery("#setZoom").val(max);
		  jQuery("#setZoom_range").val(max).trigger('input');
	  }
      map.setMinZoom(min);
      map.setMaxZoom(max);
	});
	
	/**
	 * Set zoom min/max level from input box
	 */
    jQuery('#default_min_zoom, #default_max_zoom').on('input', function() {
      let minI = parseFloat(jQuery('#default_min_zoom').val()), maxI = parseFloat(jQuery('#default_max_zoom').val());
      jQuery("#setZoom_min_range").val(minI);
      jQuery("#setZoom_max_range").val(maxI);
      jQuery("#setZoom_range").attr({"min": minI, "max": maxI});
      jQuery("#setZoom").attr({"min": minI, "max": maxI});

      if(minI >= parseFloat(jQuery("#setZoom").val())) {
          jQuery("#setZoom").val(minI);
          jQuery("#setZoom_range").val(minI).trigger('input');
      } else if(maxI <= parseFloat(jQuery("#setZoom").val())){
          jQuery("#setZoom").val(maxI);
          jQuery("#setZoom_range").val(maxI).trigger('input');
      }
      map.setMinZoom(minI);
      map.setMaxZoom(maxI);
    });

  }
  
  /**
   * Color picker for map clusters
   */
  jQuery("#color-picker-btn, .clr-picker span").on("click", function() {
	if(jQuery(".color-picker").find(".a-color-picker").length === 0) {
	    AColorPicker.from('.color-picker')
		.on('change', (picker, color) => {
		  jQuery(".clr-picker span").css("background-color", picker.color);
		  jQuery("#clusterColor").val(picker.color);
		  jQuery(".color-picker").attr('acp-color', picker.color);
		})
		.on('coloradd', (picker, color) => {
		  let cca = jQuery("#addCustomColor");
		  if(cca.val().indexOf("|"+color) === -1) {
		    cca.val(cca.val()+"|"+color); 
		    jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: cca.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-name").attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-descr").attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		}) 
		.on('colorremove', (picker, color) => {
		  let ccr = jQuery("#addCustomColor");
		  if(ccr.val().indexOf("|"+color) != -1) {
			let sc = ccr.val().replace("|"+color, "");
			ccr.val(sc);
			jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: ccr.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-name").attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-descr").attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		});

	} else {
	  jQuery(".color-picker").find(".a-color-picker").remove();
	}
  });

  jQuery(window).on("click", function(event) { 
	if(!jQuery(event.target).hasClass("a-color-picker") && jQuery(event.target).parents(".a-color-picker").length == 0 && jQuery(event.target).attr('id') != 'color-picker-btn' && !jQuery(event.target).hasClass("color-holder") && !jQuery(event.target).hasClass("a-color-picker-palette-color")) {
	  jQuery(".color-picker").find(".a-color-picker").remove();
	}
  });

  jQuery("#text-name-color-picker-btn, #map-text-color-name span").on("click", function() {
	if(jQuery(".color-picker-text-name").find(".a-color-picker").length === 0) {
	    AColorPicker.from('.color-picker-text-name')
		.on('change', (picker, color) => {
		  jQuery("#map-text-color-name span").css("background-color", picker.color);
		  jQuery("#map_name_color").val(picker.color);
		  jQuery(".color-picker-text-name").attr('acp-color', picker.color);
		})
		.on('coloradd', (picker, color) => {
		  let cca = jQuery("#addCustomColor");
		  if(cca.val().indexOf("|"+color) === -1) {
		    cca.val(cca.val()+"|"+color); 
		    jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: cca.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker-text-name"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-descr").attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker").attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		}) 
		.on('colorremove', (picker, color) => {
		  let ccr = jQuery("#addCustomColor");
		  if(ccr.val().indexOf("|"+color) != -1) {
			let sc = ccr.val().replace("|"+color, "");
			ccr.val(sc);
			jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: ccr.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker-text-name"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-descr").attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker").attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		});

	} else {
	  jQuery(".color-picker-text-name").find(".a-color-picker").remove();
	}
  });

  jQuery(window).on("click", function(event) {
	if(!jQuery(event.target).hasClass("a-color-picker") && jQuery(event.target).parents(".a-color-picker").length == 0 && jQuery(event.target).attr('id') != 'text-name-color-picker-btn' && !jQuery(event.target).hasClass("color-holder") && !jQuery(event.target).hasClass("a-color-picker-palette-color")) {
	  jQuery(".color-picker-text-name").find(".a-color-picker").remove();
	}
  });

  jQuery("#text-descr-color-picker-btn, #map-text-color-descr span").on("click", function() {
	if(jQuery(".color-picker-text-descr").find(".a-color-picker").length === 0) {
	    AColorPicker.from('.color-picker-text-descr')
		.on('change', (picker, color) => {
		  jQuery("#map-text-color-descr span").css("background-color", picker.color);
		  jQuery("#map_description_color").val(picker.color);
		  jQuery(".color-picker-text-descr").attr('acp-color', picker.color);
		})
		.on('coloradd', (picker, color) => {
		  let cca = jQuery("#addCustomColor");
		  if(cca.val().indexOf("|"+color) === -1) {
		    cca.val(cca.val()+"|"+color); 
		    jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: cca.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker-text-descr"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-name").attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker").attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		}) 
		.on('colorremove', (picker, color) => {
		  let ccr = jQuery("#addCustomColor");
		  if(ccr.val().indexOf("|"+color) != -1) {
			let sc = ccr.val().replace("|"+color, "");
			ccr.val(sc);
			jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: ccr.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker-text-descr"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker-text-name").attr('acp-palette', defaultCP+''+response);
				jQuery(".color-picker").attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		});

	} else {
	  jQuery(".color-picker-text-descr").find(".a-color-picker").remove();
	}
  });

  jQuery(window).on("click", function(event) {
	if(!jQuery(event.target).hasClass("a-color-picker") && jQuery(event.target).parents(".a-color-picker").length == 0 && jQuery(event.target).attr('id') != 'text-descr-color-picker-btn' && !jQuery(event.target).hasClass("color-holder") && !jQuery(event.target).hasClass("a-color-picker-palette-color")) {
	  jQuery(".color-picker-text-descr").find(".a-color-picker").remove();
	}
  });

});

/**
 * Get mapbox token 
 */
function getToken() {
  if(tp.length == 0){
    jQuery.ajax({
      url: obj.ajax_url,
      type : 'GET',
      data : { action : 'treweler_get_admin_token' },
      success: function( response ) {
        res = atob( response ).replace("###", "");
        tp = res.replace("###", "");
		init_map();
      }
    });
  }
}

/**
 * Initialize map
 */
function init_map() {
  jQuery("#map").html("");
  if(tp.length != 0) {

    let lat = (jQuery("#latitude").val().trim() != "" ? jQuery("#latitude").val().trim() : '40.730610'),
        lng = (jQuery("#longitude").val().trim() != "" ? jQuery("#longitude").val().trim() : '-73.935242'),
        zoom = (jQuery("#setZoom").val().trim() != "" ? jQuery("#setZoom").val() : 5),
		minZ = (parseFloat(jQuery('#setZoom_min_range').val()) != 0 ? parseFloat(jQuery('#setZoom_min_range').val()) : 0),
		maxZ = (parseFloat(jQuery('#setZoom_max_range').val()) != 0 ? parseFloat(jQuery('#setZoom_max_range').val()) : 24),
        styleType = (jQuery("#custom_style").val().trim() != "" ? jQuery("#custom_style").val() : jQuery("#map_styles").val());

    mapboxgl.accessToken = tp;
    map = new mapboxgl.Map({
      container: 'map',
      style: styleType,
      center: [lng, lat],
	  minZoom: minZ,
	  maxZoom: maxZ,
      zoom: zoom
    });

	/**
	 * Get Lat/Lng, zoom value on zoom
	 */
    map.on('zoom', function() {
      /* If latlng preview is checked */
	  if( jQuery("#latlng_map_prev").prop("checked") ) {
		let llZ = map.getCenter();
        jQuery("#longitude").val(llZ.lng);
		jQuery("#latitude").val(llZ.lat);
      }
	  /* If zoom preview is checked */
      if( jQuery("#zoom_map_prev").prop("checked") ) {
        let zoomZ = parseFloat(map.getZoom()).toFixed(2);
        if(zoomZ.trim() == "" || zoomZ.trim() == "NaN" || zoomZ == 0.00 || zoomZ == 0.23){
        	zoomZ = 0;
        }
		jQuery("#setZoom_range").val(zoomZ);
		jQuery("#setZoom").val(zoomZ);
      }
    });

    /**
     * Add Map Fullscreen Control
     */
    if( jQuery("#fullscreen").length && jQuery("#fullscreen").prop("checked") ) {
      map.addControl(new mapboxgl.FullscreenControl());
	  jQuery(map._container).find(".mapboxgl-ctrl-fullscreen").parent(".mapboxgl-ctrl-group").addClass('treweler-fs-ctrl');
    }
    /**
     * Add Map Zoom Pan Control
     */
    if( jQuery("#zoom_pan").length && jQuery("#zoom_pan").prop("checked") ) {
      map.addControl(new mapboxgl.NavigationControl()); //top-left , top-right , bottom-left , bottom-right
	  jQuery(map._container).find(".mapboxgl-ctrl-zoom-in").parent(".mapboxgl-ctrl-group").addClass('treweler-zp-ctrl');
    }
    /**
     * Add Map Diststance Scale Control
     */
    if( jQuery("#dist_scale").length && jQuery("#dist_scale").prop("checked") ) {
      map.addControl(new mapboxgl.ScaleControl({
        maxWidth: 100,
        unit: 'imperial'  	 //'imperial' , 'metric' or 'nautical'
      }), 'bottom-right');   //top-left , top-right , bottom-left , bottom-right
    }
    /**
     * Add Map Search Control
     */
    if( jQuery("#Search").length && jQuery("#Search").prop("checked") ) {
      map.addControl(
        geo = new MapboxGeocoder({
          accessToken: mapboxgl.accessToken,
          zoom: 14,
          placeholder: 'Enter search...',
          mapboxgl: mapboxgl,
		  marker: false
        }), "top-left"
      );
	  geo.on('result', function(res){
	    let latlng = res.result.center;
	  	jQuery("#latitude").val(latlng[1]);
	  	jQuery("#longitude").val(latlng[0]);
	  });
    }

	/**
	 * If 'Use the map preview' is checked for latlng
	 */
    if( jQuery("#latlng_map_prev").length && jQuery("#latlng_map_prev").prop("checked") ) {
	  jQuery("#latitude").attr('readonly', 'readonly');
	  jQuery("#longitude").attr('readonly', 'readonly');
	  
	  /* Enable Map Drag */
	  map.dragPan.enable();
	  map.dragRotate.enable();
	  map.on("drag", function(){
		let lnglatC = map.getCenter();
		jQuery("#longitude").val(lnglatC.lng);
		jQuery("#latitude").val(lnglatC.lat);
	  });
	} else {
	  jQuery("#latitude").removeAttr('readonly');
	  jQuery("#longitude").removeAttr('readonly');
	  
	  /* Disable Map Drag */
	  map.dragPan.disable();
	  map.dragRotate.disable();
	}

	/**
	 * If 'Use the map preview' is checked for zoom
	 */
    if( jQuery("#zoom_map_prev").length && jQuery("#zoom_map_prev").prop("checked") ) {
	  jQuery("#setZoom_range").attr('disabled', 'disabled');
      jQuery("#setZoom").attr('readonly', 'readonly');

	  /* Enable Map Zoom on click/scroll */
	  map.doubleClickZoom.enable();
	  map.scrollZoom.enable();
	  if(!jQuery("#latlng_map_prev").prop("checked") && jQuery("#zoom_map_prev").prop("checked")) {
	    map.on("wheel", function(e){
		  let mZoom = parseFloat(map.getZoom()).toFixed(2),
		  	latW = parseFloat(jQuery("#latitude").val()),
		  	lngW = parseFloat(jQuery("#longitude").val());
			map.scrollZoom.setWheelZoomRate(1);
		  	map.zoomTo(mZoom, {center:[lngW, latW]});
	    });
	    map.on("dblclick", function(e){ 
		  if(!jQuery("#latlng_map_prev").prop("checked") && jQuery("#zoom_map_prev").prop("checked")) {
		    e.preventDefault();
		    let dcZoom = parseInt(map.getZoom()),
		      latDC = parseFloat(jQuery("#latitude").val()),
		      lngDC = parseFloat(jQuery("#longitude").val());
		      jQuery("#setZoom").val(dcZoom);
		      jQuery("#setZoom_range").val(dcZoom);
		      e.lngLat.lng = lngDC;
		      e.lngLat.lat = latDC;
		      e._defaultPrevented = false;
		      map.doubleClickZoom.enable();
		      map.zoomTo(dcZoom, {center:[lngDC, latDC]});
		  }
	    });
	  }

	} else {
	  jQuery("#setZoom_range").removeAttr('disabled');
      jQuery("#setZoom").removeAttr('readonly');

	  /* Disable Map Zoom on click/scroll */
	  map.doubleClickZoom.disable();
	  map.scrollZoom.disable();
	}

  } else {
	jQuery("#map").append('<p class="notice notice-error settings-error is-dismissible">Mapbox access token is required.</p>');
  }
}


/**
 * `shortcode` generation dynamic events
 */
jQuery(document).ready(function() {
  if(jQuery("#sc_shortcode").length){

    jQuery("#sc_isFullwidth").on("change", function(){

      let sc = jQuery("#sc_shortcode").val(),
          fw = 'type="fullwidth"';

      if(jQuery(this).prop('checked') && sc.indexOf(fw) === -1) {
        sc = sc.splice(-1, 0, " "+fw);
        jQuery("#sc_shortcode").val(sc);
      } else {
		sc = sc.replace(" "+fw, "");
		jQuery("#sc_shortcode").val(sc);
      }

    });
	
	jQuery("#sc_isScrollZoom").on("change", function(){

      let sc = jQuery("#sc_shortcode").val(),
          sz = 'scrollzoom="no"';

      if(jQuery(this).prop('checked') && sc.indexOf(sz) === -1) {
        sc = sc.splice(-1, 0, " "+sz);
        jQuery("#sc_shortcode").val(sc);
      } else {
		sc = sc.replace(" "+sz, "");
		jQuery("#sc_shortcode").val(sc);
      }

    });

    jQuery("#sc_width, #sc_height").on("input", function(){

      let sc = jQuery("#sc_shortcode").val();

      if(jQuery(this).attr('id') === "sc_width") {

        let w = 'width="'+ jQuery(this).val() + jQuery("#sc_width_unit").val() +'"';
        if(sc.indexOf('width=') === -1) {
          sc = sc.splice(-1, 0, " "+w);
          jQuery("#sc_shortcode").val(sc);
        } else {
          let lastIdx = sc.indexOf('"', sc.indexOf('width="') + 'width="'.length);
          sc = sc.replace(sc.substring(sc.indexOf(' width="'), lastIdx+1), "");
          if( jQuery(this).val().trim() != "" && !isNaN(jQuery(this).val()) ) {
            sc = sc.splice(-1, 0, " "+w);
          }
          jQuery("#sc_shortcode").val(sc);
        }

      } else if(jQuery(this).attr('id') === "sc_height") {

        let h = 'height="'+ jQuery(this).val() + jQuery("#sc_height_unit").val() +'"';
        if(sc.indexOf('height=') === -1) {
          sc = sc.splice(-1, 0, " "+h);
          jQuery("#sc_shortcode").val(sc);
        } else {
          let lastIdx = sc.indexOf('"', sc.indexOf('height="') + 'height="'.length);
          sc = sc.replace(sc.substring(sc.indexOf(' height="'), lastIdx+1), "");
          if( jQuery(this).val().trim() != "" && !isNaN(jQuery(this).val()) ) {
            sc = sc.splice(-1, 0, " "+h);
          }
          jQuery("#sc_shortcode").val(sc);
        }

      }

    });

    jQuery("#sc_width_unit, #sc_height_unit").on("input", function(){

      let sc = jQuery("#sc_shortcode").val();

      if(jQuery(this).attr('id') === "sc_width_unit") {

        let w = 'width="'+ jQuery("#sc_width").val() + jQuery("#sc_width_unit").val() +'"';
        if(sc.indexOf('width=') === -1) {
          sc = sc.splice(-1, 0, " "+w);
          jQuery("#sc_shortcode").val(sc);
        } else {
          let lastIdx = sc.indexOf('"', sc.indexOf('width="') + 'width="'.length);
          sc = sc.replace(sc.substring(sc.indexOf(' width="'), lastIdx+1), "");
          if( jQuery("#sc_width").val().trim() != "" && !isNaN(jQuery("#sc_width").val()) ) {
            sc = sc.splice(-1, 0, " "+w);
          }
          jQuery("#sc_shortcode").val(sc);
        }

      } else if(jQuery(this).attr('id') === "sc_height_unit") {

        let h = 'height="'+ jQuery("#sc_height").val() + jQuery("#sc_height_unit").val() +'"';
        if(sc.indexOf('height=') === -1) {
          sc = sc.splice(-1, 0, " "+h);
          jQuery("#sc_shortcode").val(sc);
        } else {
          let lastIdx = sc.indexOf('"', sc.indexOf('height="') + 'height="'.length);
          sc = sc.replace(sc.substring(sc.indexOf(' height="'), lastIdx+1), "");
          if( jQuery("#sc_height").val().trim() != "" && !isNaN(jQuery("#sc_height").val()) ) {
            sc = sc.splice(-1, 0, " "+h);
          }
          jQuery("#sc_shortcode").val(sc);
        }

      }

    });

    jQuery("#copy_shortcode_btn").on("click", function(){
      document.getElementById("sc_shortcode").select();
      document.execCommand('copy');
    });

    jQuery("#sc_shortcode").on("keydown", function () {
      if(jQuery(this).attr('readonly') == undefined) {
        jQuery(this).attr("readonly", "readonly");
      }
	  return false;
    });

  }
});

/* Insert a string at a specific index */
if (!String.prototype.splice) {
    /**
     * {JSDoc}
     *
     * The splice() method changes the content of a string by removing a range of
     * characters and/or adding new characters.
     *
     * @this {String}
     * @param {number} start Index at which to start changing the string.
     * @param {number} delCount An integer indicating the number of old chars to remove.
     * @param {string} newSubStr The String that is spliced in.
     * @return {string} A new string with the spliced substring.
     */
    String.prototype.splice = function(start, delCount, newSubStr) {
        return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    };
}