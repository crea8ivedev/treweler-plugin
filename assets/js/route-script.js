var tp = '';
var map, draw, direction, lineDrawnStyle, lineDrawnVertaxPointHalosStyle, lineDrawnVertaxPointStyle, lineDrawnMidPointStyle, lineDrawnVertaxPointActiveStyle;
var checkedBtn = 'no';
var bufferData = [];

jQuery(document).ready(function() {
	if(jQuery("input[name='profile']:checked").val() == "no") {
		let lineColor = jQuery("#routeColor").val();
		lineDrawnStyle = {
				"id": "gl-draw-line",
				"type": "line",
				"filter": [
					"all", 
					["==", "$type", "LineString"], 
					["!=", "mode", "static"]
				],
				"layout": {
					"line-cap": "round",
					"line-join": "round"
				},
				"paint": {
				  'line-color': lineColor, //'#317dfc',
				  "line-dasharray": [1, 0],
				  "line-width": 3,
				  "line-opacity": 1
				}
		}
	} else {
		checkedBtn = jQuery("input[name='profile']:checked").val();
		lineDrawnStyle = {
				"id": "gl-draw-line",
				"type": "line",
				"filter": [
					"all", 
					["==", "$type", "LineString"], 
					["!=", "mode", "static"]
				],
				"layout": {
					"line-cap": "round",
					"line-join": "round"
				},
				"paint": {
				  'line-color': '#317dfc',
				  "line-dasharray": [0, 2],
				  "line-width": 2,
				  "line-opacity": 1
				}
		}
	}

	lineDrawnVertaxPointHalosStyle = {
			"id": "gl-draw-polygon-and-line-vertex-halo-active",
			"type": "circle",
			"filter": [
				"all", 
				["==", "meta", "vertex"], 
				["==", "$type", "Point"], 
				["!=", "mode", "static"]
			],
			"paint": {
				"circle-radius": 10,
				"circle-color": "#FFF"
			}
	}

	lineDrawnMidPointStyle = {
			"id": "gl-draw-polygon-and-line-vertex-active",
			"type": "circle",
			"filter": [
				"all", 
				["==", "meta", "vertex"], 
				["==", "$type", "Point"], 
				["!=", "mode", "static"]
			],
			"paint": {
				"circle-radius": 5,
				"circle-color": "#317dfc",
			}
	}

	lineDrawnVertaxPointStyle = {
            'id': 'gl-draw-polygon-and-line-midpoint-halo-active',
            'type': 'circle',
            'filter': [
              'all',
              ['==', 'meta', 'midpoint'],
              ['==', '$type', 'Point'],
              ['!=', 'mode', 'static']
            ],
            'paint': {
              'circle-radius': 0,
              'circle-color': '#FFF'
            }
    }

	lineDrawnVertaxPointActiveStyle = {
            'id': 'gl-draw-polygon-and-line-midpoint-active',
            'type': 'circle',
            'filter': [
              'all',
              ['==', 'meta', 'midpoint'],
              ['==', '$type', 'Point'],
              ['!=', 'mode', 'static']
            ],
            'paint': {
              'circle-radius': 5,
              'circle-color': '#317dfc'
            }
    }

  if(jQuery("#route_map").length) {
    setTimeout(function() { getToken(); }, 1000);
  }

  /**
   * On change of `map`, set `map` style
   */
  jQuery("#map_id").on("input", function(){
    let styleV = (jQuery("#map_id").val().trim() != "" ? jQuery("#map_id option:selected").attr('data-map_style') : "mapbox://styles/mapbox/streets-v11");
    map.setStyle(styleV);
	
	/* Place a route on change of map style */
	addRouteOnChange();
  });

  /**
   * Mapbox route profile
   */
  jQuery(".mapbox-directions-profile input[name='profile']").on("change", function() {

	if(Object.keys(bufferData).length == 0 || bufferData.features.length == 0) {
		bufferData = draw.getAll();
	}

	if(jQuery(this).val() != 'no' && checkedBtn == 'no') {
		checkedBtn = jQuery(this).val();
		lineDrawnStyle = {
			"id": "gl-draw-line",
			"type": "line",
			"filter": [
				"all", 
				["==", "$type", "LineString"], 
				["!=", "mode", "static"]
			],
			"layout": {
				"line-cap": "round",
				"line-join": "round"
			},
			"paint": {
			  'line-color': '#317dfc',
              'line-dasharray': [0, 2],
              'line-width': 2,
              'line-opacity': 1
			}
		}
		/* Add/Remove Draw Control */
		removeAddDrawControl();
		
	} else if(jQuery(this).val() == 'no' && checkedBtn != 'no') {
		
		checkedBtn = jQuery(this).val();
		let lineColor = jQuery("#routeColor").val();
		lineDrawnStyle = {
			"id": "gl-draw-line",
			"type": "line",
			"filter": [
				"all", 
				["==", "$type", "LineString"], 
				["!=", "mode", "static"]
			],
			"layout": {
				"line-cap": "round",
				"line-join": "round"
			},
			"paint": {
			  "line-color": lineColor, //"#317dfc",
              "line-dasharray": [1, 0],
              "line-width": 3,
              "line-opacity": 1
			}
		}
		/* Add/Remove Draw Control */
		removeAddDrawControl();

	}

	updateRoute();

  });

  /**
   * Route color picker
   */
  jQuery("#color-picker-btn, .clr-picker span").on("click", function() {
	if(jQuery(".color-picker").find(".a-color-picker").length === 0) {
	    AColorPicker.from('.color-picker')
		.on('change', (picker, color) => {
		  jQuery(".clr-picker span").css("background-color", picker.color);
		  jQuery("#routeColor").val(picker.color);
		  jQuery(".color-picker").attr('acp-color', picker.color);

		  if(event.type == "mousedown" || event.type == "click") {
			/* Change route color based on - color-picker */
			setTimeout(function() {
			if(jQuery("input[name='profile']:checked").val() == 'no') {
				let lineColor = jQuery("#routeColor").val();
				lineDrawnStyle = {
						"id": "gl-draw-line",
						"type": "line",
						"filter": [
							"all", 
							["==", "$type", "LineString"], 
							["!=", "mode", "static"]
						],
						"layout": {
							"line-cap": "round",
							"line-join": "round"
						},
						"paint": {
						  'line-color': lineColor, //'#317dfc',
						  "line-dasharray": [1, 0],
						  "line-width": 3,
						  "line-opacity": 1
						}
				}
				/* Add/Remove Draw Control */
				removeAddDrawControl();
			}
			updateRoute();
			}, 1000);
		  }

		})
		.on('coloradd', (picker, color) => {
		  let cca = jQuery("#addCustomColor");
		  if(cca.val().indexOf("|"+color) === -1) {
		    cca.val(cca.val()+"|"+color);
		    jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: cca.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		})
		.on('colorremove', (picker, color) => {
		  let ccr = jQuery("#addCustomColor");
		  if(ccr.val().indexOf("|"+color) != -1) {
			let sc = ccr.val().replace("|"+color, "");
			ccr.val(sc);
			jQuery.ajax({
		      url: obj.ajax_url,
		      type : 'POST',
		      data : { action : 'treweler_add_colorpicker_custom_color', cust_color: ccr.val() },
		      success: function( response ) {
				let eleCP = jQuery(".color-picker"), defaultCP = eleCP.attr('default-palette');
				eleCP.attr('acp-palette', defaultCP+''+response);
			  }
		    });
		  }
		});

	} else {
	  jQuery(".color-picker").find(".a-color-picker").remove();
	}
  });

  jQuery(window).on("click", function(event) {
	if(!jQuery(event.target).hasClass("a-color-picker") && jQuery(event.target).parents(".a-color-picker").length == 0 && jQuery(event.target).attr('id') != 'color-picker-btn' && !jQuery(event.target).hasClass("color-holder") && !jQuery(event.target).hasClass("a-color-picker-palette-color")) {
	  jQuery(".color-picker").find(".a-color-picker").remove();
	}
  });

  jQuery("#route_line_width, #route_line_opacity, #route_line_dash,#route_line_gap").on("input", function(){
	updateRoute();
  });
  
  if (wp.media) {
	var featuredGPXObj = wp.media.featuredImage.frame();
	featuredGPXObj.on('select', function(e) {
		let selectedGPX = featuredGPXObj.state().get('selection').first().toJSON(); /* selectedGPX.url */
		if(selectedGPX.subtype == "gpx+xml" && (selectedGPX.url != '' && selectedGPX.url != undefined && selectedGPX.url != null)) {
			jQuery("#routeGPXFile").val(selectedGPX.url);
			jQuery("input[name='profile']").each(function(i, e){
				if(i!=0){
					jQuery(this).attr('disabled', 'disabled');
				} else {
					jQuery(this).attr('checked', 'checked');
				}
			});
			/* Add Route */
			readGPXFileAndPlotRoute(selectedGPX.url);
		}
	});
  }
});

/**
 * Get mapbox token 
 */
function getToken() {
  if(tp.length == 0) {
    jQuery.ajax({
      url: obj.ajax_url,
      type : 'GET',
      data : { action : 'treweler_get_admin_token' },
      success: function( response ) {
        res = atob( response ).replace("###", "");
        tp = res.replace("###", "");
		init_map();
      }
    });
  }
}

/**
 * Initialize map
 */
function init_map() {
  jQuery("#route_map").html("");
  if(tp.length != 0) {

    let latlng = jQuery("#latlng").val();
		latlng = latlng.substring(1, latlng.length-1).split(",");
	let	lat = latlng[1],
        lng = latlng[0],
        zoom = (jQuery("#setZoom").val().trim() != "" ? parseFloat(jQuery("#setZoom").val()).toFixed(2) : 0),
		map_style = (jQuery("#map_id").val().trim() != "" ? jQuery("#map_id option:selected").attr('data-map_style') : "mapbox://styles/mapbox/streets-v11");

    mapboxgl.accessToken = tp;

/* 	direction = new MapboxDirections({
	    accessToken: mapboxgl.accessToken,
		unit: 'metric',
		controls: {
			inputs: true,
			instructions: false,
			profileSwitcher: true
		},
		interactive: false,


		styles: '',
		profile: 'mapbox/driving',
		alternatives: false,
		geocoder: false

	});	 */	
	draw = new MapboxDraw({
		displayControlsDefault: false,
		controls: {
			line_string: true,
			trash: true
		},
		styles: [
		  lineDrawnStyle,
		// vertex point halos
		  lineDrawnVertaxPointHalosStyle,
		// vertex points
		  lineDrawnVertaxPointStyle,
		// Mid point
		  lineDrawnMidPointStyle,
		// Mid point active
		  lineDrawnVertaxPointActiveStyle
	    ]
	});
    map = new mapboxgl.Map({
      container: 'route_map',
      style: map_style,
      center: [lng, lat],
      zoom: zoom
    });
	map.on("zoom", function(){
		jQuery("#setZoom").val(parseFloat(map.getZoom()).toFixed(2));
		let latlng = map.getCenter();
		jQuery("#latlng").val("{"+latlng.lng+","+latlng.lat +"}");
	});
	map.on("drag", function(){
		let latlng = map.getCenter();
		jQuery("#latlng").val("{"+latlng.lng+","+latlng.lat +"}");
	});
//	map.addControl(direction, 'top-left');
	map.addControl(draw, 'top-right');
	map.on('draw.create', updateRoute);
	map.on('draw.update', updateRoute);
	map.on('draw.delete', removeRouteData);

	map.on("load", function(){
		if(jQuery("#routeCoords").val().trim() != "" && jQuery("#routeGPXFile").val().trim() == "") {
			let setRoute = jQuery("#routeCoords").val(), defaultCoords = [];
			setRoute = setRoute.split(";");
			setRoute.forEach(function(ele){
				let ll = [];
				ele.split(",").forEach(function(l){
					ll.push(parseFloat(l));
				});
				defaultCoords.push(ll);
			});

			bufferData = {
				'type': "FeatureCollection",
				'features': [
					{
						'type': "Feature",
						'properties': {},
						'geometry': {
							'coordinates': defaultCoords,
							'type': 'LineString'
						}
					}
				]
			}
			var profile = jQuery("input[name='profile']:checked").val(); /* Set the profile */
			draw.set(bufferData);
			data = draw.getAll();
			if(data.features.length > 0) {
				var lastFeature = 0;
				var coords = data.features[lastFeature].geometry.coordinates;
				/* Format the coordinates */
				var newCoords = coords.join(';');
				/* Set the radius for each coordinate pair to 25 meters */
				var radius = [];
				coords.forEach(element => {
				  radius.push(25);
				});
				if(profile != 'no') {
				  getMatchDistance(newCoords, radius, profile);
				} else {
				  addRoute(data.features[lastFeature].geometry);
				}
			}
		} else if(jQuery("#routeGPXFile").val().trim() != "") {
			let GPXFile = jQuery("#routeGPXFile").val();
			jQuery("input[name='profile']").each(function(i, e){
				if(i!=0){
					jQuery(this).attr('disabled', 'disabled');
				} else {
					jQuery(this).attr('checked', 'checked');
				}
			});
			/* Add Route */
			readGPXFileAndPlotRoute(GPXFile);
		}
	});
  } else {
	jQuery("#route_map").append('<p class="notice notice-error settings-error is-dismissible">Mapbox access token is required.</p>');
  }
}

/**
 * Use the coordinates you just drew to make the Map Matching API request
 */
function updateRoute(e) {
	removeRoute(); /* Overwrite any existing layers */

	var profile = jQuery("input[name='profile']:checked").val(); /* Set the profile */

	jQuery(".post-type-route .info-box #directions").html("");
	if(jQuery("#routeGPXFile").val().trim() == "") {
		/* Get the coordinates */
		if(Object.keys(bufferData).length > 0 && bufferData.features.length > 0 && e == undefined) {
			draw.set(bufferData);
		/*	draw.changeMode('draw_point'); */

			data = draw.getAll();
			if(data.features.length > 0) {
				var lastFeature = 0;
				var coords = data.features[lastFeature].geometry.coordinates;
				/* Format the coordinates */
				var newCoords = coords.join(';');
				/* Set the radius for each coordinate pair to 25 meters */
				var radius = [];
				coords.forEach(element => {
				  radius.push(25);
				});
				if(profile != 'no') {
				  getMatchDistance(newCoords, radius, profile);
				} else {
				  addRoute(data.features[lastFeature].geometry);
				}
			}
		} else {
			var data = draw.getAll();
			bufferData = data;
			if(data.features.length > 0) {
				var lastFeature = data.features.length - 1;
				var coords = data.features[lastFeature].geometry.coordinates;
				/* Format the coordinates */
				var newCoords = coords.join(';');
				/* Set the radius for each coordinate pair to 25 meters */
				var radius = [];
				coords.forEach(element => {
				  radius.push(25);
				});
				if(profile != 'no') {
				  getMatchDistance(newCoords, radius, profile);
				} else {
				  addRoute(data.features[lastFeature].geometry);
				}
			}
		}
		if(data.features.length > 0) {
			var coords = data.features[0].geometry.coordinates.join(';');
			jQuery("#routeCoords").val(coords);
		}
	} else {
		let selectedGPX = jQuery("#routeGPXFile").val();
		readGPXFileAndPlotRoute(selectedGPX);
	}
	
}

/**
 * Make a Map Distance request
 */
function getMatchDistance(coordinates, radius, profile) {
	/* Separate the radiuses with semicolons */
	var radiuses = radius.join(';');

	var query = 'https://api.mapbox.com/directions/v5/mapbox/' +
				profile + '/' +
				coordinates + '?' +
			/*	'alternatives=true&' + */
				'geometries=geojson&steps=true' + 
			/*	'&radiuses=' + radiuses + */
				'&access_token=' + mapboxgl.accessToken;

	jQuery.ajax({
	  method: 'GET',
	  url: query
	}).done(function(data) {
	  if(data.routes.length > 0) {
		var coords = data.routes[0].geometry;
	    addRoute(coords);
	/*	getInstructions(data.routes[0]); */
	  } else {
		jQuery(".post-type-route .info-box #directions").html('<span id="direction-error">'+data.message+'</span>');
		console.log(data.code +': '+data.message);
	  } 
	}).fail(function (jqXHR, textStatus) {
		jQuery(".post-type-route .info-box #directions").html('<span id="direction-error">'+jqXHR.responseJSON.message+'</span>');
		console.log(jqXHR.responseJSON.code +': '+jqXHR.responseJSON.message);
	});
}

/**
 * Create the query - Matching API
 */
function getMatch(coordinates, radius, profile) {
	/* Separate the radiuses with semicolons */
	var radiuses = radius.join(';');

	var query = 'https://api.mapbox.com/matching/v5/mapbox/' +
				profile + '/' +
				coordinates + '?' +
				'geometries=geojson' +
				'&radiuses=' + radiuses +
				'&access_token=' + mapboxgl.accessToken;

	jQuery.ajax({
	  method: 'GET',
	  url: query
	}).done(function(data) {
	  if(data.matchings.length > 0) {
	    var coords = data.matchings[0].geometry;
	    addRoute(coords);
	/*	getInstructions(data.matchings[0]); */
	  } else {
		jQuery(".post-type-route .info-box #directions").html('<span id="direction-error">'+data.message+'</span>');
		console.log(data.code +': '+data.message);
	  }
	});
}

/**
 * Get instructions for route
 */
function getInstructions(data) {
	/* Target the sidebar to add the instructions */
	var directions = document.getElementById('directions');
	var legs = data.legs;
	var tripDirections = "";
	/* Output the instructions for each step of each leg in the response object */
	tripDirections += "<ul>";
	for (var i = 0; i < legs.length; i++) {
	  var steps = legs[i].steps;
	  for (var j = 0; j < steps.length; j++) {
		tripDirections  += "<li>" + steps[j].maneuver.instruction + "</li>";
	  }
	}
	tripDirections += "</ul>";

	var TDh = Math.floor(data.duration / 3600);
	var TDm = Math.floor((data.duration - (TDh * 3600)) / 60);
	directions.innerHTML = '<h2>Trip duration: '+ TDh +' h ' + TDm + ' min. ('+ Math.floor(data.distance * 0.001) +' km)</h2>' +  tripDirections;
}

/**
 * Draw the Map Matching route as a new layer on the map
 */
function addRoute(coords) {
	/* If a route is already loaded, remove it */
	if (map.getSource('route')) {
	  jQuery(".post-type-route .info-box #directions").html("");
	  map.removeLayer('route');
	  map.removeSource('route');  
	}
	/* Add new route */
	let lineColor = jQuery("#routeColor").val(),
		lineOpacity = parseFloat(jQuery("#route_line_opacity").val()),
		lineWidth   = parseInt(jQuery("#route_line_width").val().trim() != "" ? jQuery("#route_line_width").val() : 1),
		lineDash    = parseInt(jQuery("#route_line_dash").val().trim() != "" ? jQuery("#route_line_dash").val() : 1),
		lineGap     = parseInt(jQuery("#route_line_gap").val().trim() != "" ? jQuery("#route_line_gap").val() : 0);

		lineOpacity = (lineOpacity >= 0 && lineOpacity <= 1) ? lineOpacity : 0;

	map.addLayer({
	  'id': 'route',
	  'type': 'line',
	  'source': {
	    'type': 'geojson',
	    'data': {
	  	'type': 'Feature',
	  	'properties': {},
	  	'geometry': coords
	    }
	  },
	  'layout': {
	    'line-join': 'round',
	    'line-cap': 'round'
	  },
	  'paint': {
	    'line-color': lineColor,
	    'line-width': lineWidth,
	    'line-opacity': lineOpacity,
		'line-dasharray': [lineDash,lineGap]
	  }
	});
}

function addRouteOnChange() {
  data = draw.getAll();
  if(data.features.length > 0) {
  	var lastFeature = 0;
  	var coords = data.features[lastFeature].geometry.coordinates;
  	/* Format the coordinates */
  	var newCoords = coords.join(';');
  	/* Set the radius for each coordinate pair to 25 meters */
  	var radius = [];
  	coords.forEach(element => {
  	  radius.push(25);
  	});
  	let profile = jQuery("input[name='profile']:checked").val();
  	if(profile != 'no') {
  	  getMatchDistance(newCoords, radius, profile);
  	} else {
  	  addRoute(data.features[lastFeature].geometry);
  	}
  }
}

/**
 * Remove Draw control & Add again with updated value
 */
function removeAddDrawControl() {
	map.removeControl(draw);
	draw = new MapboxDraw({
		displayControlsDefault: false,
		controls: {
			line_string: true,
			trash: true
		},
		styles: [lineDrawnStyle, lineDrawnVertaxPointHalosStyle, lineDrawnVertaxPointStyle, lineDrawnMidPointStyle, lineDrawnVertaxPointActiveStyle]
	});
	map.addControl(draw);
}

/** 
 * Remove route layer from the map
 */
function removeRoute(e) {
	if (map.getSource('route') != undefined) {
	  jQuery(".post-type-route .info-box #directions").html("");

	  map.removeLayer('route');
	  map.removeSource('route');
	  jQuery("#routeCoords").val("");
	} else {
	  return;
	}
}

/** 
 * Remove route layer & data from the map
 */
function removeRouteData(e) {
	if (map.getSource('route')) {
	  jQuery(".post-type-route .info-box #directions").html("");

	  map.removeLayer('route');
	  map.removeSource('route');
	  bufferData = [];
	  jQuery("#routeCoords").val("");

	  jQuery("input[name='profile']").each(function(i, e){
	    if(i!=0){
	    	jQuery(this).removeAttr('disabled');
	    }
	  });

	} else {
	  return;
	}
}

/**
 * Read GPX file and draw route on map based on coordinates
 */
function readGPXFileAndPlotRoute(selectedGPX){
	jQuery.ajax({
		type: "GET",
		url: selectedGPX,
		dataType: "xml",
		success: function(GPX){
			var points = [];
			jQuery(GPX).find("trkpt").each(function () {
				points.push([parseFloat(jQuery(this).attr("lon")), parseFloat(jQuery(this).attr("lat"))]);
			});
			if(points.length) {
				let lineColor = jQuery("#routeColor").val(),
					lineOpacity = parseFloat(jQuery("#route_line_opacity").val()),
					lineWidth   = parseInt(jQuery("#route_line_width").val().trim() != "" ? jQuery("#route_line_width").val() : 1),
					lineDash    = parseInt(jQuery("#route_line_dash").val().trim() != "" ? jQuery("#route_line_dash").val() : 1),
					lineGap     = parseInt(jQuery("#route_line_gap").val().trim() != "" ? jQuery("#route_line_gap").val() : 0);

					lineOpacity = (lineOpacity >= 0 && lineOpacity <= 1) ? lineOpacity : 0;

				lineDrawnStyle = {
					"id": "gl-draw-line",
					"type": "line",
					"filter": [
						"all", 
						["==", "$type", "LineString"], 
						["!=", "mode", "static"]
					],
					"layout": {
						"line-cap": "round",
						"line-join": "round"
					},
					"paint": {
					  "line-color": lineColor, //"#317dfc",
					  "line-dasharray": [lineDash,lineGap],
					  "line-width": lineWidth,
					  "line-opacity": lineOpacity
					}
				}
				/* Add/Remove Draw Control */
				removeAddDrawControl();

				bufferData = {
					'type': "FeatureCollection",
					'features': [
						{
							'type': "Feature",
							'properties': {},
							'geometry': {
								'coordinates': points,
								'type': 'LineString'
							}
						}
					]
				}
				draw.set(bufferData);
				data = draw.getAll();
				addRoute(data.features[0].geometry);
			}
		}
	})
}