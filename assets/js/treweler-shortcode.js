/* Hex to RGBA color */
function hex2rgba(hex,opacity){
	hex = hex.replace('#','');
	r = parseInt(hex.substring(0,2), 16);
	g = parseInt(hex.substring(2,4), 16);
	b = parseInt(hex.substring(4,6), 16);

	return 'rgba('+r+','+g+','+b+','+opacity+')';
}

/* HTML Cluster Element */
function createNewCluster(props) {
	var html = '<div class=\"treweler-marker-cluster\"><div class=\"marker marker--cluster\" >'
			+'<div class=\"marker-wrap\">'
				+'<div class=\"marker__shadow\" style=\"border-color:'+ hex2rgba(props.color, 0.1) +'\">'
					+'<div class=\"marker__border\" style=\"border-color:'+ hex2rgba(props.color, 0.4) +'\">'
						+'<div class=\"marker__center\" style=\"background-color:'+props.color+';\">'+ props.point_count +'</div>'
					+'</div>'
				+'</div>'
			+'</div>'
		+'</div></div>';

	var el = document.createElement('div');
	el.innerHTML = html;

	return el.firstChild;
}

/* HTML Marker Element */
function createNewMarker(props) {
  if(props.icon == '' || props.icon == undefined) {

	let isDark = (props.type=='dark' ? 'marker--dark':'');
	var html = '<div class=\"treweler-marker\"><div class=\"marker '+ isDark +'\">'
		+'<div class=\"marker-wrap\">'
			+'<div class=\"marker__shadow\">'
				+'<div class=\"marker__border\"  style=\"border-color:'+ props.color +';\">'
					+'<div class=\"marker__center\"></div>'
				+'</div>'
			+'</div>'
		+'</div>'
	+'</div></div>';
	var el = document.createElement('div');
		el.innerHTML = html;
		let elementObj = {
				element: el
			}
		return elementObj;

  } else {

	/* var html = '<div class=\"treweler-marker\" style=\"background-image:url('+ props.icon +');background-size:contain;background-repeat:no-repeat;background-position: center top;height:42px;width:42px;top:-21px;left:0px;\"></div>';
	var el = document.createElement('div');
	el.innerHTML = html;
	return el.firstChild; */

	let size = props.size!='' ? props.size.split(';') : '42;42'.split(';');

	var el = document.createElement('div');
		el.className = 'treweler-marker icon';
		el.style.backgroundImage = 'url('+props.icon+')';
		el.style.backgroundSize = 'contain';
		el.style.backgroundRepeat = 'no-repeat';
		el.style.backgroundPosition = 'center top';
		el.style.width = (parseInt(size[1])<=42 ? (parseInt(size[1])%2==0 ? parseInt(size[1]) : parseInt(size[1])+1) : 42 ) +'px';
		el.style.height = (parseInt(size[0])<=42 ? (parseInt(size[0])%2==0 ? parseInt(size[0]) : parseInt(size[0])+1) : 42 ) +'px';
		let elementObj = {
				element: el,
				anchor: props.anchor,
			/*	offset:[-size[1]/2, -size[0]] */
			}
		return elementObj;

  }
}