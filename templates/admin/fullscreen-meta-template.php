<?php
/**
 * Template for fullscreen map meta attribute in page settings.
 */
$setMap = get_post_meta(get_the_ID(), '_treweler_cpt_dd_box_fullscreen', true);
$args = array(  
  'post_type'      => 'map',
  'post_status'    => 'publish',
  'posts_per_page' => -1, 
  'orderby'        => 'title', 
  'order'          => 'ASC'
);

$maps = new WP_Query( $args ); 
?>
<div class="components-base-control editor-page-attributes__template">
  <div class="components-base-control__field">
	<select name='treweler_cpt_dd_box_fullscreen' id='treweler_cpt_dd_box_fullscreen' class="components-select-control__input">
      <option value=""><?php echo esc_attr_e('-- select fullscreen map --', TREWELER_TEXT_DOMAIN); ?></option>
      <?php if( isset($maps->posts) && is_array($maps->posts) ) { foreach($maps->posts as $p) { ?>
		<option value="<?=$p->ID?>" <?php echo $setMap == $p->ID ? 'selected' :''; ?>><?php echo esc_attr($p->post_title); ?></option>
	  <?php } } /* wp_reset_postdata(); */  ?>
	</select>
  </div>
</div>


