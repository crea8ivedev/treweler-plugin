<?php
$setMap = get_post_meta(get_the_ID(), '_treweler_marker_map_id', true);
$args = array(  
  'post_type'      => 'map',
  'post_status'    => 'publish',
  'posts_per_page' => -1, 
  'orderby'        => 'title', 
  'order'          => 'ASC'
);
$maps = new WP_Query( $args );


$cust = get_post_meta(get_the_ID());

$marker_style = isset($cust["_treweler_marker_style"])? trim($cust["_treweler_marker_style"][0]):"";

$marker_ll = isset($cust["_treweler_marker_latlng"])? unserialize($cust["_treweler_marker_latlng"][0]):[];
if(!empty($marker_ll)) {
  foreach($marker_ll as $k => $v) {
    ${"ll$k"} = $v;
  }
}
if( (isset($ll0) && trim($ll0) =="") || !isset($ll0) ) {
  $ll0 = 0;
}
if( (isset($ll1) && trim($ll1) =="") || !isset($ll1) ) {
  $ll1 = 0;
}

$marker_zoom = isset($cust["_treweler_marker_zoom"])? $cust["_treweler_marker_zoom"][0] : 0;

$marker_color = isset($cust["_treweler_marker_color"])? $cust["_treweler_marker_color"][0] : '#4b7715';

$custom_color = get_option('treweler_mapbox_colorpicker_custom_color');
$defaultColors = "#F44336|#EC407A|#E046C6|#B94AEF|#8559FF|#317DFC|#426D7E|#027F71|#008A43|#238C28|#4B7715|#756B11|#C06018|#9B5A45|#505050|#00B0F6|#00C5AF|#00BC5B|#18AF1F|#5DA900|#A19100|#FF7814|#FF5D28|#FFFFFF|#000000|";

$markerIconSize = isset($cust["_treweler_marker_icon_size"])? $cust["_treweler_marker_icon_size"][0] : "";
?>

<div class="treweler-controls">
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Marker Map", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p>
    <select name="map_id" id="map_id" class="large-select">
      <option value=""><?php echo esc_attr_e('No map selected', TREWELER_TEXT_DOMAIN); ?></option>
	  <?php if( isset($maps->posts) && is_array($maps->posts) ) { foreach($maps->posts as $p) { 
	    $map_style = trim(get_post_meta($p->ID, '_treweler_map_custom_style', true))!="" ? get_post_meta($p->ID, '_treweler_map_custom_style', true) : get_post_meta($p->ID, '_treweler_map_styles', true);
		?>
      <option value="<?=$p->ID?>" data-map_style="<?=$map_style?>" <?php echo ($setMap==$p->ID ?'selected':'');?>><?php echo esc_attr($p->post_title); ?></option>
	  <?php } } ?>
    </select>
  </p>
  <hr/>

  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Marker Style", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p>
    <select name="marker_style" id="marker_style" class="large-select">
      <option value="light" <?=$marker_style=='light'?'selected':''?>>Light</option>
      <option value="dark" <?=$marker_style=='dark'?'selected':''?>>Dark</option>
    </select>
  </p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Marker Coordinates", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="text" name="latlng[0]" id="latitude" class="large-text" value="<?php echo esc_attr_e($ll0, TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("Latitude ", TREWELER_TEXT_DOMAIN);?>" /></p>
  <p><input type="text" name="latlng[1]" id="longitude" class="large-text" value="<?php echo esc_attr_e($ll1, TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("Longitude", TREWELER_TEXT_DOMAIN);?>" /></p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Marker color", TREWELER_TEXT_DOMAIN); ?></label></p>
  <div class="clr-picker">
  <span class="color-holder" style="background-color:<?=$marker_color?>;"></span><input type="button" id="color-picker-btn" value="Select Color">
  </div>
  <div class="color-picker" 
     acp-color="<?=$marker_color?>"
     acp-palette="<?=$defaultColors . $custom_color?>"
	 default-palette="<?=$defaultColors?>"
     acp-show-rgb="no"
     acp-show-hsl="no"
     acp-palette-editable>
  </div>
  <input type="hidden" name="marker_icon_size" id="marker_icon_size" value="<?php echo esc_attr_e($markerIconSize, TREWELER_TEXT_DOMAIN); ?>">
  <input type="hidden" name="addCustomColor" id="addCustomColor" value="<?php echo esc_attr_e($custom_color, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="markerColor" id="markerColor" value="<?php echo esc_attr_e($marker_color, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="setZoom" id="setZoom" value="<?php echo esc_attr_e($marker_zoom, TREWELER_TEXT_DOMAIN); ?>" />
  <br/>
</div>