<?php
global $post;
$cust = get_post_meta($post->ID);
$map_ctrl = isset($cust["_treweler_map_controls"])? unserialize($cust["_treweler_map_controls"][0]):[];
if(!empty($map_ctrl)) {
  foreach($map_ctrl as $k => $v) {
    ${"ckd$k"} = $v;
  }
}

$styles = isset($cust["_treweler_map_styles"])? trim($cust["_treweler_map_styles"][0]):"";
$map_cust_style = isset($cust["_treweler_map_custom_style"])? $cust["_treweler_map_custom_style"][0] :"";

$map_ll = isset($cust["_treweler_map_latlng"])? unserialize($cust["_treweler_map_latlng"][0]):[];
if(!empty($map_ll)) {
  foreach($map_ll as $k => $v) {
    ${"ll$k"} = $v;
  }
}

$map_zoom = isset($cust["_treweler_map_zoom"])? $cust["_treweler_map_zoom"][0] : 0;
$map_min_zoom = isset($cust["_treweler_map_min_zoom"])? $cust["_treweler_map_min_zoom"][0] : 0;
$map_max_zoom = isset($cust["_treweler_map_max_zoom"])? $cust["_treweler_map_max_zoom"][0] : 24;

$zoom_prev = isset($cust["_treweler_map_zoom_prev"])? $cust["_treweler_map_zoom_prev"][0] : 1;
$latlng_prev = isset($cust["_treweler_map_latlng_prev"])? $cust["_treweler_map_latlng_prev"][0] : 1;

$cluster_color = isset($cust["_treweler_map_cluster_color"])? $cust["_treweler_map_cluster_color"][0] : '#4b7715';
$custom_color = get_option('treweler_mapbox_colorpicker_custom_color');
$defaultColors = "#F44336|#EC407A|#E046C6|#B94AEF|#8559FF|#317DFC|#426D7E|#027F71|#008A43|#238C28|#4B7715|#756B11|#C06018|#9B5A45|#505050|#00B0F6|#00C5AF|#00BC5B|#18AF1F|#5DA900|#A19100|#FF7814|#FF5D28|#FFFFFF|#000000|";
?>

<div class="treweler-controls">
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Controls", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="checkbox" name="map_controls[0]" id="dist_scale" value="<?php echo esc_attr_e('Distance scale', TREWELER_TEXT_DOMAIN); ?>" <?php echo (isset($ckd0)?'checked':''); ?> /><?php echo esc_attr_e("Distance Scale", TREWELER_TEXT_DOMAIN); ?></p>
  <p><input type="checkbox" name="map_controls[1]" id="fullscreen" value="<?php echo esc_attr_e('Fullscreen', TREWELER_TEXT_DOMAIN); ?>" <?php echo (isset($ckd1)?'checked':''); ?> /><?php echo esc_attr_e("Fullscreen", TREWELER_TEXT_DOMAIN); ?></p>
  <p><input type="checkbox" name="map_controls[2]" id="Search" value="<?php echo esc_attr_e('Search', TREWELER_TEXT_DOMAIN); ?>" <?php echo (isset($ckd2)?'checked':''); ?> /><?php echo esc_attr_e("Search", TREWELER_TEXT_DOMAIN); ?></p>
  <p><input type="checkbox" name="map_controls[3]" id="zoom_pan" value="<?php echo esc_attr_e('Zoom & Pan', TREWELER_TEXT_DOMAIN); ?>" <?php echo (isset($ckd3)?'checked':''); ?> /><?php echo esc_attr_e("Zoom & Pan", TREWELER_TEXT_DOMAIN); ?></p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Styles", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p>
    <select name="map_styles" id="map_styles" class="large-select">
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/streets-v11', 					TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/streets-v11" 				? 'selected' :''); ?>>Streets</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/outdoors-v11', 					TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/outdoors-v11" 				? 'selected' :''); ?>>Outdoors</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/light-v10', 						TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/light-v10" 					? 'selected' :''); ?>>Streets Light</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/dark-v10', 						TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/dark-v10" 					? 'selected' :''); ?>>Streets Dark</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/satellite-v9', 					TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/satellite-v9" 				? 'selected' :''); ?>>Satellite</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/satellite-streets-v11', 			TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/satellite-streets-v11" 		? 'selected' :''); ?>>Satellite Streets</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/navigation-preview-day-v4', 		TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/navigation-preview-day-v4" 	? 'selected' :''); ?>>Navigations Preview Day</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/navigation-preview-night-v4', 	TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/navigation-preview-night-v4" ? 'selected' :''); ?>>Navigations Preview Night</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/navigation-guidance-day-v4', 	TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/navigation-guidance-day-v4" 	? 'selected' :''); ?>>Navigations Guidance Day</option>
      <option value="<?php echo esc_attr_e('mapbox://styles/mapbox/navigation-guidance-night-v4', 	TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($styles == "mapbox://styles/mapbox/navigation-guidance-night-v4"? 'selected' :''); ?>>Navigations Guidance Night</option>
    </select>
  </p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Custom Style", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="text" name="custom_style" id="custom_style" class="large-text" value="<?php echo esc_attr_e($map_cust_style, TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("E.g. mapbox://styles/username/*", TREWELER_TEXT_DOMAIN);?>" /></p>
  <hr/>

  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Cluster Color", TREWELER_TEXT_DOMAIN); ?></label></p>
  <div class="clr-picker">
  <span class="color-holder" style="background-color:<?=$cluster_color?>;"></span><input type="button" id="color-picker-btn" value="Select Color">
  </div>
  <div class="color-picker" 
     acp-color="<?=$cluster_color?>"
     acp-palette="<?=$defaultColors . $custom_color?>"
	 default-palette="<?=$defaultColors?>"
     acp-show-rgb="no"
     acp-show-hsl="no"
     acp-palette-editable>
  </div>
  <input type="hidden" name="addCustomColor" id="addCustomColor" value="<?php echo esc_attr_e($custom_color, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="clusterColor" id="clusterColor" value="<?php echo esc_attr_e($cluster_color, TREWELER_TEXT_DOMAIN); ?>" />
  <hr/>

  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Initial Point", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="text" name="default_latlng[0]" id="latitude" class="large-text" value="<?php echo esc_attr_e((isset($ll0)?$ll0:40.730610), TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("Latitude ", TREWELER_TEXT_DOMAIN);?>" /></p>
  <p><input type="text" name="default_latlng[1]" id="longitude" class="large-text" value="<?php echo esc_attr_e((isset($ll1)?$ll1:-73.935242), TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("Longitude", TREWELER_TEXT_DOMAIN);?>" /></p>
  <p><input type="checkbox" name="latlng_map_prev" id="latlng_map_prev" value="1" <?php echo (trim($latlng_prev)=="" || $latlng_prev==1 ? 'checked' : ''); ?> /><?php echo esc_attr_e("Use the map preview", TREWELER_TEXT_DOMAIN); ?></p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Initial Zoom Level", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p>
    <input type="range" step="0.01" min="<?= $map_min_zoom ?>" max="<?= $map_max_zoom ?>" name="default_zoom_range" id="setZoom_range" class="large-text" value="<?php echo esc_attr_e($map_zoom, TREWELER_TEXT_DOMAIN); ?>" />
    <input type="number" step="0.01" min="<?= $map_min_zoom ?>" max="<?= $map_max_zoom ?>" name="default_zoom" id="setZoom" class="large-text" value="<?php echo esc_attr_e($map_zoom, TREWELER_TEXT_DOMAIN); ?>" />
  </p>
  <p class="zoom-map-prev-chk"><input type="checkbox" name="zoom_map_prev" id="zoom_map_prev" value="1" <?php echo (trim($zoom_prev)=="" || $zoom_prev==1 ? 'checked' : ''); ?> /><?php echo esc_attr_e("Use the map preview", TREWELER_TEXT_DOMAIN); ?></p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Zoom Range", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p class="multirange">
    <input type="range" step="0.01" min="0" max="24" name="min_zoom_range" id="setZoom_min_range" class="large-text lower-slider" value="<?php echo esc_attr_e($map_min_zoom, TREWELER_TEXT_DOMAIN); ?>" />
    <input type="range" step="0.01" min="0" max="24" name="max_zoom_range" id="setZoom_max_range" class="large-text upper-slider" value="<?php echo esc_attr_e($map_max_zoom, TREWELER_TEXT_DOMAIN); ?>" />
  </p>
  <p class="zoom-ctrl-label">
	<input type="number" step="0.01" min="0" max="24" id="default_min_zoom" class="alighleft" value="<?= $map_min_zoom ?>" />
	<input type="number" step="0.01" min="0" max="24" id="default_max_zoom" class="alignright" value="<?= $map_max_zoom ?>" />
  </p> 
  <br/>
</div>