<?php
global $post;
$cust = get_post_meta($post->ID);

$scIsFullwidth = isset($cust["_treweler_map_sc_isFullwidth"])? $cust["_treweler_map_sc_isFullwidth"][0] : 0;

$scWidth = isset($cust["_treweler_map_sc_width"])? $cust["_treweler_map_sc_width"][0] : '';
$scWidth_unit = isset($cust["_treweler_map_sc_width_unit"])? $cust["_treweler_map_sc_width_unit"][0] : '';

$scHeight = isset($cust["_treweler_map_sc_height"])? $cust["_treweler_map_sc_height"][0] : '';
$scHeight_unit = isset($cust["_treweler_map_sc_height_unit"])? $cust["_treweler_map_sc_height_unit"][0] : '';

$scShortcode = isset($cust["_treweler_map_sc_shortcode"])? $cust["_treweler_map_sc_shortcode"][0] : '[treweler map-id="'. $post->ID .'"]';
$scIsScrollZoom = isset($cust["_treweler_map_sc_scroll_zoom"])? $cust["_treweler_map_sc_scroll_zoom"][0] : false;
?>

<div class="treweler-controls">

  <p><input type="checkbox" name="sc_isFullwidth" id="sc_isFullwidth" value="1" <?php  echo ($scIsFullwidth==1 ? 'checked':''); ?> /><?php echo esc_attr_e("Fullwidth Map", TREWELER_TEXT_DOMAIN); ?></p>
  <p><input type="checkbox" name="sc_isScrollZoom" id="sc_isScrollZoom" value="true" <?php  echo ($scIsScrollZoom==true ? 'checked':''); ?> /><?php echo esc_attr_e("Disable scroll zoom", TREWELER_TEXT_DOMAIN); ?></p>

  <p>
    <input type="text" name="sc_width" id="sc_width" class="medium-text" value="<?php echo esc_attr_e($scWidth, TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("Width", TREWELER_TEXT_DOMAIN);?>" />
	<select name="sc_width_unit" id="sc_width_unit" class="small-select">
      <option value="<?php echo esc_attr_e('%',  TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($scWidth_unit == "%" 	? 'selected' :''); ?>>%</option>
      <option value="<?php echo esc_attr_e('px', TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($scWidth_unit == "px" ? 'selected' :''); ?>>px</option>
      <option value="<?php echo esc_attr_e('em', TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($scWidth_unit == "em" ? 'selected' :''); ?>>em</option>
    </select>
  </p>

  <p>
    <input type="text" name="sc_height" id="sc_height" class="medium-text" value="<?php echo esc_attr_e($scHeight, TREWELER_TEXT_DOMAIN); ?>" placeholder="<?php echo esc_attr_e("Height", TREWELER_TEXT_DOMAIN);?>" />
    <select name="sc_height_unit" id="sc_height_unit" class="small-select">
      <option value="<?php echo esc_attr_e('px', TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($scHeight_unit == "px" ? 'selected' :''); ?>>px</option>
      <option value="<?php echo esc_attr_e('em', TREWELER_TEXT_DOMAIN ); ?>" <?php echo ($scHeight_unit == "em" ? 'selected' :''); ?>>em</option>
    </select>
  </p>

  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Shortcode", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><textarea name="sc_shortcode" id="sc_shortcode" class="large-text" rows="4" placeholder="<?php echo esc_attr_e("Copy shortcode from here", TREWELER_TEXT_DOMAIN);?>" readonly="readonly"><?php echo esc_attr_e($scShortcode, TREWELER_TEXT_DOMAIN); ?></textarea></p>
  <!-- <p><button id="copy-shortcode-btn" data-clipboard-action="copy" class="button button-large">Copy to Clipboard</button></p> -->
  <p><input type="button" id="copy_shortcode_btn" value="Copy to Clipboard" data-clipboard-action="copy" class="button button-large"></p>

  <hr/>
  <p class="post-attributes-label-wrapper"><label><?php echo esc_attr_e("Use the page settings to add a fullscreen map.", TREWELER_TEXT_DOMAIN); ?></label></p>

  <br/>
</div>