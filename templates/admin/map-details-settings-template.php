<?php
$name = get_post_meta(get_the_ID(), '_treweler_map_details_name', true);
$name_color = get_post_meta(get_the_ID(), '_treweler_map_details_name_color', true);
$name_color = $name_color!=""?$name_color:'#3C3D35';
$desc = get_post_meta(get_the_ID(), '_treweler_map_details_description', true);
$desc_color = get_post_meta(get_the_ID(), '_treweler_map_details_description_color', true);
$desc_color = $desc_color!=""?$desc_color:'#3C3D35';
$position = get_post_meta(get_the_ID(), '_treweler_map_details_position', true);
$custom_color = get_option('treweler_mapbox_colorpicker_custom_color');
$defaultColors = "#F44336|#EC407A|#E046C6|#B94AEF|#8559FF|#317DFC|#426D7E|#027F71|#008A43|#238C28|#4B7715|#756B11|#C06018|#9B5A45|#505050|#00B0F6|#00C5AF|#00BC5B|#18AF1F|#5DA900|#A19100|#FF7814|#FF5D28|#FFFFFF|#000000|";
?>
<!------
<div class="treweler-controls">
    <table class="form-table">
      <tr>
          <th width="18%">
            <label for="treweler_mapbox_access_token">
              <?php echo esc_attr_e('Map Name', TREWELER_TEXT_DOMAIN); ?>
            </label>
          </th>
          <td width="67%">
            <input type="text" name="map_name" id="map_name" class="large-text" value="<?= esc_attr_e(htmlspecialchars($name), TREWELER_TEXT_DOMAIN) ?>">
          </td>
		  <td width="15%">
			<div class="map-text-color" id="map-text-color-name">
				<span class="color-holder" style="background-color:<?=$name_color?>;"></span><input type="button" id="text-name-color-picker-btn" class="text-color-picker-btn" value="Select Color">
			</div>
			<div class="color-picker-text-name" 
			 acp-color="<?=$name_color?>"
			 acp-palette="<?=$defaultColors . $custom_color?>"
			 default-palette="<?=$defaultColors?>"
			 acp-show-rgb="no"
			 acp-show-hsl="no"
			 acp-palette-editable>
		    </div>
			<input type="hidden" name="map_name_color" class="input-color" id="map_name_color" value="<?=$name_color?>">
		  </td>
      </tr>
	  <tr>
          <th width="18%">
            <label for="treweler_mapbox_access_token">
              <?php echo esc_attr_e('Map Description', TREWELER_TEXT_DOMAIN); ?>
            </label>
          </th>
          <td width="67%">
            <input type="text" name="map_description" id="map_description" class="large-text" value="<?= esc_attr_e(htmlspecialchars($desc), TREWELER_TEXT_DOMAIN) ?>">
          </td>
		  <td width="15%">
			<div class="map-text-color" id="map-text-color-descr">
				<span class="color-holder" style="background-color:<?=$desc_color?>;"></span><input type="button" id="text-descr-color-picker-btn" class="text-color-picker-btn" value="Select Color">
			</div>
			<div class="color-picker-text-descr" 
			 acp-color="<?=$desc_color?>"
			 acp-palette="<?=$defaultColors . $custom_color?>"
			 default-palette="<?=$defaultColors?>"
			 acp-show-rgb="no"
			 acp-show-hsl="no"
			 acp-palette-editable>
		    </div>
			<input type="hidden" name="map_description_color" class="input-color" id="map_description_color" value="<?=$desc_color?>">
		  </td>
      </tr>
	  <tr>
          <th width="18%">
            <label for="treweler_mapbox_access_token">
              <?php echo esc_attr_e('Position', TREWELER_TEXT_DOMAIN); ?>
            </label>
          </th>
          <td width="67%">
            <select class="large-select" name="descr_position" id="descr_position">
				<option value="top_left" <?= $position=='top_left'?'selected':'' ?>>Top Left</option>
				<option value="top_right" <?= $position=='top_right'?'selected':'' ?>>Top Right</option>
				<option value="bottom_left" <?= $position=='bottom_left'?'selected':'' ?>>Bottom Left</option>
				<option value="bottom_right" <?= $position=='bottom_right'?'selected':'' ?>>Bottom Right</option>
			</select>
          </td>
		  <td width="15%">
		  </td>
	  </tr>
    </table>
</div>
--->
<style>
#treweler_map_description_settings_controls-meta .inside {
	margin:0;
	padding:0;
}
.treweler-setting-controls {
	overflow: hidden;
}
.treweler-setting-controls ul.treweler-settings-tab-list {
	margin: 0;
    width: 20%;
    float: left;
    line-height: 1em;
    padding: 0 0 10px;
    position: relative;
    background-color: #fafafa;
    border-right: 1px solid #eee;
    box-sizing: border-box;
}
.treweler-setting-controls ul.treweler-settings-tab-list::after {
	content: "";
    display: block;
    width: 100%;
    height: 9999em;
    position: absolute;
    bottom: -9999em;
    left: 0;
    background-color: #fafafa;
    border-right: 1px solid #eee;
}
.treweler-setting-controls ul.treweler-settings-tab-list li {
	margin: 0;
    padding: 0;
    display: block;
    position: relative;
}
.treweler-setting-controls ul.treweler-settings-tab-list li a {
	margin: 0;
    padding: 10px;
    display: block;
    box-shadow: none;
    text-decoration: none;
    line-height: 20px!important;
    border-bottom: 1px solid #eee;
}
.treweler-setting-controls ul.treweler-settings-tab-list li.active a {
	color: #555;
    position: relative;
    background-color: #eee;
}
.treweler-setting-controls ul.treweler-settings-tab-list li a span{
	margin-left: .618em;
    margin-right: .618em;
}
.treweler-setting-controls .tab-panel {
	float: left;
    width: 78%;
    padding: 0px 8px;
}
.treweler-setting-controls .tab-panel .form-table tr th, 
.treweler-setting-controls .tab-panel .form-table tr td {
	padding: 10px 5px;
	vertical-align: middle;
}
</style>
<div class="treweler-setting-controls">
	<ul class="treweler-settings-tab-list" role="tablist" aria-orientation="vertical">
		<li role="presentation" class="active"><a href="#general" role="tab"><span>General</span></a></li>
		<li role="presentation"><a href="#settings" role="tab"><span>Settings</span></a></li>
		<li role="presentation"><a href="#shortcode" role="tab"><span>Shortcode</span></a></li>
	</ul>

	<div id="general" class="tab-panel" style="display: block;" role="tabpanel">

		<table class="form-table">
		  <tr>
			  <th width="25%">
				<label for="treweler_mapbox_access_token">
				  <?php echo esc_attr_e('Map Name', TREWELER_TEXT_DOMAIN); ?>
				</label>
			  </th>
			  <td width="60%">
				<input type="text" name="map_name" id="map_name" class="large-text" value="<?= esc_attr_e(htmlspecialchars($name), TREWELER_TEXT_DOMAIN) ?>">
			  </td>
			  <td width="15%">
				<div class="map-text-color" id="map-text-color-name">
					<span class="color-holder" style="background-color:<?=$name_color?>;"></span><input type="button" id="text-name-color-picker-btn" class="text-color-picker-btn" value="Select Color">
				</div>
				<div class="color-picker-text-name" 
				 acp-color="<?=$name_color?>"
				 acp-palette="<?=$defaultColors . $custom_color?>"
				 default-palette="<?=$defaultColors?>"
				 acp-show-rgb="no"
				 acp-show-hsl="no"
				 acp-palette-editable>
				</div>
				<input type="hidden" name="map_name_color" class="input-color" id="map_name_color" value="<?=$name_color?>">
			  </td>
		  </tr>
		  <tr>
			  <th width="25%">
				<label for="treweler_mapbox_access_token">
				  <?php echo esc_attr_e('Map Description', TREWELER_TEXT_DOMAIN); ?>
				</label>
			  </th>
			  <td width="60%">
				<input type="text" name="map_description" id="map_description" class="large-text" value="<?= esc_attr_e(htmlspecialchars($desc), TREWELER_TEXT_DOMAIN) ?>">
			  </td>
			  <td width="15%">
				<div class="map-text-color" id="map-text-color-descr">
					<span class="color-holder" style="background-color:<?=$desc_color?>;"></span><input type="button" id="text-descr-color-picker-btn" class="text-color-picker-btn" value="Select Color">
				</div>
				<div class="color-picker-text-descr" 
				 acp-color="<?=$desc_color?>"
				 acp-palette="<?=$defaultColors . $custom_color?>"
				 default-palette="<?=$defaultColors?>"
				 acp-show-rgb="no"
				 acp-show-hsl="no"
				 acp-palette-editable>
				</div>
				<input type="hidden" name="map_description_color" class="input-color" id="map_description_color" value="<?=$desc_color?>">
			  </td>
		  </tr>
		  <tr>
			  <th width="25%">
				<label for="treweler_mapbox_access_token">
				  <?php echo esc_attr_e('Position', TREWELER_TEXT_DOMAIN); ?>
				</label>
			  </th>
			  <td width="60%">
				<select class="large-select" name="descr_position" id="descr_position">
					<option value="top_left" <?= $position=='top_left'?'selected':'' ?>>Top Left</option>
					<option value="top_right" <?= $position=='top_right'?'selected':'' ?>>Top Right</option>
					<option value="bottom_left" <?= $position=='bottom_left'?'selected':'' ?>>Bottom Left</option>
					<option value="bottom_right" <?= $position=='bottom_right'?'selected':'' ?>>Bottom Right</option>
				</select>
			  </td>
			  <td width="15%">
			  </td>
		  </tr>
		</table>

	</div>
	<div id="settings" class="tab-panel" style="display: none;" role="tabpanel">
	
	</div>
	<div id="shortcode" class="tab-panel" style="display: none;" role="tabpanel">
		
	</div>
</div>