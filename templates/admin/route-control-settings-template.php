<?php
$setMap = get_post_meta(get_the_ID(), '_treweler_route_map_id', true);
$args = array(  
  'post_type'      => 'map',
  'post_status'    => 'publish',
  'posts_per_page' => -1, 
  'orderby'        => 'title', 
  'order'          => 'ASC'
);
$maps = new WP_Query( $args );

$cust = get_post_meta(get_the_ID());

$routeLineWidth = (isset($cust["_treweler_route_line_width"]) && trim($cust["_treweler_route_line_width"][0])!="") ? $cust["_treweler_route_line_width"][0] : 3;
$routeLineOpacity = (isset($cust["_treweler_route_line_opacity"]) && trim($cust["_treweler_route_line_opacity"][0])!="") ? $cust["_treweler_route_line_opacity"][0] : 1;
$routeLineDash = (isset($cust["_treweler_route_line_dash"]) && trim($cust["_treweler_route_line_dash"][0])!="") ? $cust["_treweler_route_line_dash"][0] : 1;
$routeLineGap = (isset($cust["_treweler_route_line_gap"]) && trim($cust["_treweler_route_line_gap"][0])!="") ? $cust["_treweler_route_line_gap"][0] : 0;

$route_latlng = isset($cust["_treweler_route_map_latlng"])? $cust["_treweler_route_map_latlng"][0] : '{0.1,0.1}';
$route_zoom = isset($cust["_treweler_route_map_zoom"])? $cust["_treweler_route_map_zoom"][0] : 0;
$route_color = isset($cust["_treweler_route_line_color"])? $cust["_treweler_route_line_color"][0] : '#438EE4';
$route_coords = isset($cust["_treweler_route_line_coords"])? $cust["_treweler_route_line_coords"][0] : '';
$route_gpx = isset($cust["_treweler_route_gpx_file"])? $cust["_treweler_route_gpx_file"][0] : '';

$custom_color = get_option('treweler_mapbox_colorpicker_custom_color');
$defaultColors = "#F44336|#EC407A|#E046C6|#B94AEF|#8559FF|#317DFC|#426D7E|#027F71|#008A43|#238C28|#4B7715|#756B11|#C06018|#9B5A45|#505050|#00B0F6|#00C5AF|#00BC5B|#18AF1F|#5DA900|#A19100|#FF7814|#FF5D28|#FFFFFF|#000000|";
?>

<div class="treweler-controls">
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Route map", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p>
    <select name="map_id" id="map_id" class="large-select">
      <option value=""><?php echo esc_attr_e('No map selected', TREWELER_TEXT_DOMAIN); ?></option>
	  <?php if( isset($maps->posts) && is_array($maps->posts) ) { foreach($maps->posts as $p) { 
	    $map_style = trim(get_post_meta($p->ID, '_treweler_map_custom_style', true))!="" ? get_post_meta($p->ID, '_treweler_map_custom_style', true) : get_post_meta($p->ID, '_treweler_map_styles', true);
		?>
      <option value="<?=$p->ID?>" data-map_style="<?=$map_style?>" <?php echo ($setMap==$p->ID ?'selected':'');?>><?php echo esc_attr($p->post_title); ?></option>
	  <?php } } ?>
    </select>
  </p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Route line color", TREWELER_TEXT_DOMAIN); ?></label></p>
  <div class="clr-picker">
  <span class="color-holder" style="background-color:<?=$route_color?>;"></span><input type="button" id="color-picker-btn" value="Select Color">
  </div>
  <div class="color-picker" 
     acp-color="<?=$route_color?>"
     acp-palette="<?=$defaultColors . $custom_color?>"
	 default-palette="<?=$defaultColors?>"
     acp-show-rgb="no"
     acp-show-hsl="no"
     acp-palette-editable>
  </div>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Route line width, px", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="text" name="route_line_width" id="route_line_width" class="large-text" value="<?=$routeLineWidth?>" placeholder=""></p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Route line opacity", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="text" name="route_line_opacity" id="route_line_opacity" class="large-text" value="<?=$routeLineOpacity?>" placeholder=""><small>Value between 0 and 1 (Example 0.8)</small></p>
  <hr/>
  <p class="post-attributes-label-wrapper"><label class="post-attributes-label"><?php echo esc_attr_e("Dash and gap", TREWELER_TEXT_DOMAIN); ?></label></p>
  <p><input type="text" name="route_line_dash" id="route_line_dash" class="half-text alignleft" value="<?=$routeLineDash?>" placeholder="Dash"><input type="text" name="route_line_gap" id="route_line_gap" class="half-text alignright" value="<?=$routeLineGap?>" placeholder="Gap"></p>

  <input type="hidden" name="addCustomColor" id="addCustomColor" value="<?php echo esc_attr_e($custom_color, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="routeColor" id="routeColor" value="<?php echo esc_attr_e($route_color, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="setZoom" id="setZoom" value="<?php echo esc_attr_e($route_zoom, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="latlng" id="latlng" value="<?php echo esc_attr_e($route_latlng, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="routeCoords" id="routeCoords" value="<?php echo esc_attr_e($route_coords, TREWELER_TEXT_DOMAIN); ?>" />
  <input type="hidden" name="routeGPXFile" id="routeGPXFile" value="<?php echo esc_attr_e($route_gpx, TREWELER_TEXT_DOMAIN); ?>" />
  <br/>
  <br/>
</div>