<div class='wrap'>
  <h2>Treweler Settings</h2>
  <form method="post" action="options.php"><?php 
    settings_fields('treweler_mapbox_access_token');
    $options = get_option('treweler_mapbox_access_token');?>
    <table class="form-table">
      <tr>
          <th>
            <label for="treweler_mapbox_access_token">
              <?php echo esc_attr_e('Access Token', TREWELER_TEXT_DOMAIN); ?>
            </label>
          </th>
          <td>
            <input type="text" name="treweler_mapbox_access_token" id="treweler_mapbox_access_token" class="large-text" value="<?= $options ?>">
            <p class="description">Enter a valid <a href="https://account.mapbox.com/access-tokens/" target="_blank">Mapbox access token</a>.</p>
          </td>
      </tr>
      <?php do_settings_fields('treweler_mapbox_access_token', 'default') ?>
    </table>
    <?php do_settings_sections('treweler_mapbox_access_token') ?>
    <?php submit_button() ?>
  </form>
</div>