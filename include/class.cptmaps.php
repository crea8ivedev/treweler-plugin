<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('cptMaps') ) :

require_once( TREWELER_PLUGIN_DIR . 'include/class.cptmarkers.php' );
require_once( TREWELER_PLUGIN_DIR . 'include/class.cptroutes.php' );

class cptMaps extends cptMapMarkers {
  private static $initiated = false;
  private $routes;

  function __construct() {
    if(! self::$initiated) {
	  parent::__construct();
	  $this->routes = new cptMapRoutes();
      self::treweler_cpt_maps_init();
    }
  }

  function treweler_cpt_maps_init() {
    add_action( 'init', array($this, 'treweler_cpt_maps') );
    add_action( 'admin_menu', array($this, 'treweler_hide_add_new_main_menu') );
	add_action( 'edit_form_after_title', array($this, 'add_placeholder_event') );

	add_action( 'add_meta_boxes', array($this, 'treweler_add_meta_init') ); //admin_init
	add_action( 'save_post_'. TREWELER_PLUGIN_CPT_MAP, array($this, 'treweler_save_meta_details'), 10, 3 );

	add_action( 'add_meta_boxes', array($this, 'treweler_add_shortcode_meta') ); //admin_init
	add_action( 'save_post_'. TREWELER_PLUGIN_CPT_MAP, array($this, 'treweler_save_shortcode_meta_details'), 10, 3 );

	add_action( 'add_meta_boxes', array($this, 'treweler_add_map_description_meta') ); //admin_init
	add_action( 'save_post_'. TREWELER_PLUGIN_CPT_MAP, array($this, 'treweler_save_map_description_meta_details'), 10, 3 );

	add_action( 'add_meta_boxes', array($this, 'treweler_cpt_dd_meta_box_add') );
	add_action( 'save_post', array($this, 'treweler_cpt_dd_save_meta_details') );
	
	add_filter( 'manage_'. TREWELER_PLUGIN_CPT_MAP .'_posts_columns' , array($this,'treweler_map_post_type_columns') );
	add_action( 'manage_'. TREWELER_PLUGIN_CPT_MAP .'_posts_custom_column' , array($this,'treweler_map_fill_map_id_column'), 10, 2 );
	
	add_filter( 'get_user_option_meta-box-order_' . TREWELER_PLUGIN_CPT_MAP, array($this,'treweler_map_metabox_order') );

  }

  function treweler_cpt_maps() {
    $labels = array(
      'name'                  => _x( 'Maps', 'Post type general name', TREWELER_TEXT_DOMAIN ),
      'singular_name'         => _x( 'Map', 'Post type singular name', TREWELER_TEXT_DOMAIN ),
      'add_new'               => __( 'Add New', TREWELER_TEXT_DOMAIN ),
      'add_new_item'          => __( 'Add New Map', TREWELER_TEXT_DOMAIN ),
      'edit_item'             => __( 'Edit Map', TREWELER_TEXT_DOMAIN ),
      'new_item'              => __( 'New Map', TREWELER_TEXT_DOMAIN ),
      'view_item'             => __( 'View Map', TREWELER_TEXT_DOMAIN ),
      'all_items'             => __( 'Maps', TREWELER_TEXT_DOMAIN ),
      'search_items'          => __( 'Search Maps', TREWELER_TEXT_DOMAIN ),
      'not_found'             => __( 'No Maps found.', TREWELER_TEXT_DOMAIN ),
      'not_found_in_trash'    => __( 'No Maps found in Trash.', TREWELER_TEXT_DOMAIN ),  
      'menu_name'             => _x( 'Treweler', 'map', TREWELER_TEXT_DOMAIN ),
      'name_admin_bar'        => _x( 'Map', 'Add New on Toolbar', TREWELER_TEXT_DOMAIN ),
      'parent_item_colon'     => __( 'Parent Maps:', TREWELER_TEXT_DOMAIN ),

	  'featured_image'        => _x( 'Map Logo', TREWELER_TEXT_DOMAIN ),
      'set_featured_image'    => _x( 'Upload map logo', TREWELER_TEXT_DOMAIN ),
      'remove_featured_image' => _x( 'Remove map logo', TREWELER_TEXT_DOMAIN ),
	  'use_featured_image'    => _x( 'Use as map logo', TREWELER_TEXT_DOMAIN ),

      'archives'              => _x( 'Map archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'insert_into_item'      => _x( 'Insert into map', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'uploaded_to_this_item' => _x( 'Uploaded to this map', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'filter_items_list'     => _x( 'Filter Maps list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'items_list_navigation' => _x( 'Maps list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'items_list'            => _x( 'Maps list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
    );

    $args = array(
      'labels'              => $labels,
      'description'         => 'Describe travels, routes, locations and other interesting events that can be described using maps.',
      'public'              => false,
      'publicly_queryable'  => false,
      'query_var'           => true,
      'show_ui'             => true,
      'show_in_menu'        => true,
      'menu_icon'           => 'dashicons-admin-site',
      'show_in_nav_menus'   => true,
      'capability_type'     => 'post',
      'has_archive'         => false,
      'hierarchical'        => false,
      'can_export'          => true,
      'menu_position'       => null,
      'rewrite'             => array( 'slug' => null, 'with_front' => false),
      'exclude_from_search' => false,
      'supports'            => array( 'title', 'page-attributes', 'thumbnail' ), //'custom-fields', 
    );
    register_post_type( 'map', $args );
  }

  function treweler_hide_add_new_main_menu() {
    global $submenu;
    unset($submenu['edit.php?post_type=map'][10]);
  }

  /**
   * Treweler `map` settings functionality
   */
  function treweler_add_meta_init() {
    add_meta_box("treweler_map_settings_controls-meta", esc_html__( "Map Settings", TREWELER_TEXT_DOMAIN ), array($this, "treweler_map_settings_controls"), "map", "side", "core");
  }

  function treweler_map_settings_controls() {
    require TREWELER_PLUGIN_DIR . 'templates/admin/control-settings-template.php';
  }

  function treweler_save_meta_details($post_id, $post, $update) {

	if( !empty($_POST) && TREWELER_PLUGIN_CPT_MAP == $post->post_type ) {

		update_post_meta($post->ID, "_treweler_map_controls", $_POST["map_controls"]);

		if(isset($_POST["map_styles"]))
			update_post_meta($post->ID, "_treweler_map_styles", sanitize_text_field($_POST["map_styles"]));
		if(isset($_POST["custom_style"]))
			update_post_meta($post->ID, "_treweler_map_custom_style", sanitize_text_field($_POST["custom_style"]));
		if(isset($_POST["default_latlng"]))
			update_post_meta($post->ID, "_treweler_map_latlng", $_POST["default_latlng"]);
		if(isset($_POST["default_zoom"]))
			update_post_meta($post->ID, "_treweler_map_zoom", $_POST["default_zoom"]);
		if(isset($_POST["min_zoom_range"]))
			update_post_meta($post->ID, "_treweler_map_min_zoom", $_POST["min_zoom_range"]);
		if(isset($_POST["max_zoom_range"]))
			update_post_meta($post->ID, "_treweler_map_max_zoom", $_POST["max_zoom_range"]);
		if(isset($_POST["clusterColor"]))
			update_post_meta($post->ID, "_treweler_map_cluster_color", $_POST["clusterColor"]);

			update_post_meta($post->ID, "_treweler_map_zoom_prev", isset($_POST["zoom_map_prev"]) ? $_POST["zoom_map_prev"] : 0);
			update_post_meta($post->ID, "_treweler_map_latlng_prev", isset($_POST["latlng_map_prev"]) ? $_POST["latlng_map_prev"] : 0);
	} else {
		return;
	}
  }

  /**
   * Treweler `shortcode` settings functionality
   */
  function treweler_add_shortcode_meta () {
	add_meta_box("treweler_map__shortcode_settings_controls-meta", esc_html__( "Map Shortcode", TREWELER_TEXT_DOMAIN ), array($this, "treweler_map__shortcode_settings_controls"), "map", "side", "core");
  }

  function treweler_map__shortcode_settings_controls() {
    require TREWELER_PLUGIN_DIR . 'templates/admin/shortcode-settings-template.php';
  }

  function treweler_save_shortcode_meta_details($post_id, $post, $update) {
    if( !empty($_POST) && TREWELER_PLUGIN_CPT_MAP == $post->post_type ) {

		update_post_meta($post->ID, "_treweler_map_sc_isFullwidth", isset($_POST["sc_isFullwidth"]) ? $_POST["sc_isFullwidth"] : 0);
		$zoom = isset($_POST["sc_isScrollZoom"]) ? true : false;
		update_post_meta($post->ID, "_treweler_map_sc_scroll_zoom", $zoom);

		if(isset($_POST["sc_width"]))
			update_post_meta($post->ID, "_treweler_map_sc_width", sanitize_text_field($_POST["sc_width"]));
		if(isset($_POST["sc_width_unit"]))
			update_post_meta($post->ID, "_treweler_map_sc_width_unit", sanitize_text_field($_POST["sc_width_unit"]));
		if(isset($_POST["sc_height"]))
			update_post_meta($post->ID, "_treweler_map_sc_height", sanitize_text_field($_POST["sc_height"]));
		if(isset($_POST["sc_height_unit"]))
			update_post_meta($post->ID, "_treweler_map_sc_height_unit", sanitize_text_field($_POST["sc_height_unit"]));
		if(isset($_POST["sc_shortcode"]))
			update_post_meta($post->ID, "_treweler_map_sc_shortcode", $_POST["sc_shortcode"]);

    }
  }

  /**
   * Treweler map title & description settings
   */
  function treweler_add_map_description_meta() {
	add_meta_box("treweler_map_description_settings_controls-meta", esc_html__( "Map Details", TREWELER_TEXT_DOMAIN ), array($this, "treweler_map_description_settings_controls"), "map", "normal", "default");
  }

  function treweler_map_description_settings_controls() {
    require TREWELER_PLUGIN_DIR . 'templates/admin/map-details-settings-template.php';
  }

  function treweler_save_map_description_meta_details($post_id, $post, $update) {
    if( !empty($_POST) && TREWELER_PLUGIN_CPT_MAP == $post->post_type ) {

		if(isset($_POST["map_name"]))
			update_post_meta($post->ID, "_treweler_map_details_name", $_POST["map_name"]);
		if(isset($_POST["map_description"]))
			update_post_meta($post->ID, "_treweler_map_details_description", $_POST["map_description"]);
		if(isset($_POST["map_name_color"]))
			update_post_meta($post->ID, "_treweler_map_details_name_color", sanitize_text_field($_POST["map_name_color"]));
		if(isset($_POST["map_description_color"]))
			update_post_meta($post->ID, "_treweler_map_details_description_color", sanitize_text_field($_POST["map_description_color"]));
		if(isset($_POST["descr_position"]))
			update_post_meta($post->ID, "_treweler_map_details_position", $_POST["descr_position"]);

    }
  }

  /**
   * Treweler map container block - map edit
   */
  function add_placeholder_event() {
    $post_type = get_post_type();
    if( $post_type == TREWELER_PLUGIN_CPT_MAP ) {
      echo '<div id="map" class="treweler-mapbox"></div>';
    }
  }

  /**
   * Treweler `fullscreen` map dropdown settings - page
   */
  function treweler_cpt_dd_meta_box_add() {
    add_meta_box("treweler_map_cpt_dd_box-meta", esc_html__( "Treweler Fullscreen Map", TREWELER_TEXT_DOMAIN ), array($this, "treweler_map_cpt_dd_box"), "page", "side", "high", null);
    /* add_filter('use_block_editor_for_post', '__return_false', 10); */
  }

  function treweler_map_cpt_dd_box() {
    require TREWELER_PLUGIN_DIR . 'templates/admin/fullscreen-meta-template.php';
  }

  function treweler_cpt_dd_save_meta_details() {
	if( !empty($_POST) && $_POST['post_type'] == 'page' ) {
	  if( isset($_POST["treweler_cpt_dd_box_fullscreen"]) ) {
	  	// delete_post_meta($_POST['post_ID'], "_treweler_cpt_dd_box_fullscreen");
	  	update_post_meta($_POST['post_ID'], "_treweler_cpt_dd_box_fullscreen", sanitize_text_field($_POST["treweler_cpt_dd_box_fullscreen"]));
	  }
	} else {
		return;
	}
  }
  
  /**
   * Add custom column in post type - `map` table
   */
  function treweler_map_post_type_columns($columns){
	return array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Title'),
		'post_id' =>__( 'Map ID'),
		'date' =>__( 'Date')
	);
  }

  function treweler_map_fill_map_id_column($column, $post_id){
	switch ( $column ) {
		case 'post_id' :
			echo $post_id; 
			break;
	}
  }

  function treweler_map_metabox_order ( $order ){
	$order['side'] = explode(',', $order['side']);
	$feature = array_search("postimagediv", $order['side']);
	$lastMap = array_search("treweler_map__shortcode_settings_controls-meta", $order['side']);
	
	$firstA = array_slice($order['side'], 0, $feature);
	$middleA = array_slice($order['side'], $feature+1, $lastMap);
	$reOrder = array_splice($order['side'], $feature, 1);
	$lastA = array_slice($order['side'], $lastMap, end($order['side']));

	$order['side'] = array_merge($firstA, $middleA, $reOrder, $lastA);
	$order['side'] = implode(',', $order['side']);

	return $order;
  }

}

endif;