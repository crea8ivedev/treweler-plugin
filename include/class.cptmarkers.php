<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('cptMapMarkers') ) :

class cptMapMarkers {
  private static $initiated = false;

  function __construct() {
    if(! self::$initiated) {
      self::treweler_cpt_markers_init();
    }
  }

  function treweler_cpt_markers_init() {
    add_action( 'init', array($this, 'treweler_cpt_markers') );
    add_action( 'edit_form_after_title', array($this, 'add_placeholder_map_for_marker') );

    add_action( 'add_meta_boxes', array($this, 'treweler_add_marker_meta_init') ); //admin_init
    add_action( 'save_post_'. TREWELER_PLUGIN_CPT_MARKER, array($this, 'treweler_save_marker_meta_details'), 10, 3 );

    add_filter( 'manage_'. TREWELER_PLUGIN_CPT_MARKER .'_posts_columns' , array($this,'treweler_marker_post_type_columns') );
    add_action( 'manage_'. TREWELER_PLUGIN_CPT_MARKER .'_posts_custom_column' , array($this,'treweler_marker_fill_map_id_column'), 10, 2 );

	add_filter( 'admin_post_thumbnail_html', array($this, 'treweler_map_marker_position_settings_controls'), 10, 2 );
  }

  function treweler_cpt_markers() {
    $labels = array(
      'name'                  => _x( 'Markers', 'Post type general name', TREWELER_TEXT_DOMAIN ),
      'singular_name'         => _x( 'Marker', 'Post type singular name', TREWELER_TEXT_DOMAIN ),
      'add_new'               => __( 'Add New', TREWELER_TEXT_DOMAIN ),
      'add_new_item'          => __( 'Add New Marker', TREWELER_TEXT_DOMAIN ),
      'edit_item'             => __( 'Edit Marker', TREWELER_TEXT_DOMAIN ),
      'new_item'              => __( 'New Marker', TREWELER_TEXT_DOMAIN ),
      'view_item'             => __( 'View Marker', TREWELER_TEXT_DOMAIN ),
      'all_items'             => __( 'Markers', TREWELER_TEXT_DOMAIN ),
      'search_items'          => __( 'Search Markers', TREWELER_TEXT_DOMAIN ),
      'not_found'             => __( 'No Markers found.', TREWELER_TEXT_DOMAIN ),
      'not_found_in_trash'    => __( 'No Markers found in Trash.', TREWELER_TEXT_DOMAIN ),  
      'menu_name'             => _x( 'Treweler', 'marker', TREWELER_TEXT_DOMAIN ),
      'name_admin_bar'        => _x( 'Marker', 'Add New on Toolbar', TREWELER_TEXT_DOMAIN ),
      'parent_item_colon'     => __( 'Parent Markers:', TREWELER_TEXT_DOMAIN ),

	  'featured_image'        => _x( 'Custom Marker', TREWELER_TEXT_DOMAIN ),
      'set_featured_image'    => _x( 'Upload custom marker', TREWELER_TEXT_DOMAIN ),
      'remove_featured_image' => _x( 'Remove custom marker', TREWELER_TEXT_DOMAIN ),
	  'use_featured_image'    => _x( 'Use as marker icon', TREWELER_TEXT_DOMAIN ),

      'archives'              => _x( 'Marker archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'insert_into_item'      => _x( 'Insert into marker', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'uploaded_to_this_item' => _x( 'Uploaded to this marker', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'filter_items_list'     => _x( 'Filter Markers list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'items_list_navigation' => _x( 'Markers list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'items_list'            => _x( 'Markers list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
    );

    $args = array(
      'labels'              => $labels,
      'description'         => 'Describe travels, routes, locations and other interesting events that can be described using maps.',
      'public'              => false,
      'publicly_queryable'  => false,
      'query_var'           => true,
      'show_ui'             => true,
      'show_in_menu'        => 'edit.php?post_type=map',
      'show_in_nav_menus'   => true,
      'capability_type'     => 'post',
      'has_archive'         => false,
      'hierarchical'        => false,
      'can_export'          => true,
      'menu_position'       => null,
      'rewrite'             => array( 'slug' => null, 'with_front' => false),
      'exclude_from_search' => false,
      'supports'            => array( 'title', 'page-attributes', 'thumbnail' ), //'custom-fields', 
    );
    register_post_type( 'marker', $args );
  }

  /**
   * Add custom column in post type - `marker` table
   */
  function treweler_marker_post_type_columns($columns){
	return array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Title'),
		'map_title' =>__( 'Marker Map'),
		'date' =>__( 'Date')
	);
  }

  function treweler_marker_fill_map_id_column($column, $post_id){
	switch ( $column ) {
		case 'map_title' :
			if(get_post_meta($post_id, '_treweler_marker_map_id', true) != ""){
			  echo '<a href="'. admin_url() .'post.php?post='. get_post_meta($post_id, '_treweler_marker_map_id', true) .'&action=edit">'. get_the_title( get_post_meta($post_id, '_treweler_marker_map_id', true) ) .'</a>'; 	
			}
			break;
	}
  }

  /**
   * Treweler map container block - marker edit
   */
  function add_placeholder_map_for_marker() {
    $post_type = get_post_type();
    if( $post_type == TREWELER_PLUGIN_CPT_MARKER ) {
      echo '<div id="marker_map" class="treweler-mapbox"></div>';
    }
  }

  /**
   * Treweler `marker` settings functionality
   */
  function treweler_add_marker_meta_init() {
    add_meta_box("treweler_map_marker_settings_controls-meta", esc_html__( "Marker Settings", TREWELER_TEXT_DOMAIN ), array($this, "treweler_map_marker_settings_controls"), "marker", "side", "core");
  }

  function treweler_map_marker_settings_controls() {
    require TREWELER_PLUGIN_DIR . 'templates/admin/marker-control-settings-template.php';
  }

  function treweler_save_marker_meta_details($post_id, $post, $update) {
	if( !empty($_POST) && TREWELER_PLUGIN_CPT_MARKER == $post->post_type ) {

		if(isset($_POST["map_id"]))
			update_post_meta($post->ID, "_treweler_marker_map_id", sanitize_text_field($_POST["map_id"]));
		if(isset($_POST["marker_style"]))
			update_post_meta($post->ID, "_treweler_marker_style", sanitize_text_field($_POST["marker_style"]));
		if(isset($_POST["latlng"]))
			update_post_meta($post->ID, "_treweler_marker_latlng", $_POST["latlng"]);
		if(isset($_POST["setZoom"]))
			update_post_meta($post->ID, "_treweler_marker_zoom", $_POST["setZoom"]);
		if(isset($_POST["markerColor"]))
			update_post_meta($post->ID, "_treweler_marker_color", $_POST["markerColor"]);
		if(isset($_POST["marker_position"]))
			update_post_meta($post->ID, "_treweler_marker_position", $_POST["marker_position"]);
		if(isset($_POST["marker_icon_size"]))
			update_post_meta($post->ID, "_treweler_marker_icon_size", $_POST["marker_icon_size"]);

	} else {
		return;
	}
  }

  /**
   * Add custom meta for marker position
   */
  function treweler_map_marker_position_settings_controls( $content, $post_id ) {
	if(get_post_type($post_id) == TREWELER_PLUGIN_CPT_MARKER) {
	  $field_id    = 'show_featured_image';
	  $getSaved = get_post_meta($post_id, '_treweler_marker_position', true);
	  $getSaved = $getSaved != "" ? $getSaved : 'Center';
	  $field_label = sprintf(
	      '<p><label for="%1$s" class="post-attributes-label">Marker Position</label></p>
	  	<p><select name="marker_position" id="marker_position" class="large-select">
	  		<option value="center" '. ($getSaved=="center"?"selected":"") .'>Center</option>
	  		<option value="top" '. ($getSaved=="top"?"selected":"") .'>Top</option>
	  		<option value="bottom" '. ($getSaved=="bottom"?"selected":"") .'>Bottom</option>
	  		<option value="left" '. ($getSaved=="left"?"selected":"") .'>Left</option>
	  		<option value="right" '. ($getSaved=="right"?"selected":"") .'>Right</option>
	  		<option value="top-left" '. ($getSaved=="top-left"?"selected":"") .'>Top-left</option>
	  		<option value="top-right" '. ($getSaved=="top-right"?"selected":"") .'>Top-right</option>
	  		<option value="bottom-left" '. ($getSaved=="bottom-left"?"selected":"") .'>Bottom-left</option>
	  		<option value="bottom-right" '. ($getSaved=="bottom-right"?"selected":"") .'>Bottom-right</option>
	  	</select></p>',
	      $field_id
	  );
	  return $content .= $field_label;
	}
	return $content;
  }
}

endif;