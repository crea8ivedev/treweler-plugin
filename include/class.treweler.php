<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('treweler') ) :

require_once( TREWELER_PLUGIN_DIR . 'include/class.cptmaps.php' );

class treweler extends cptMaps {

  private static $initiated = false;

  public function __construct() {
    if(! self::$initiated) {
      parent::__construct();
      self::treweler_init_hooks();
	  if (is_admin()) {
		add_action( 'admin_footer', array($this, 'treweler_remove_table_control'));
	  }
	}
  }

  public function treweler_init_hooks() {
    add_action( 'admin_menu',                       			 array($this, 'treweler_plugin_main_menu')    			);
    add_action( 'admin_init',                       			 array($this, 'treweler_register_settings')   			);
    add_action( 'admin_notices',                    			 array($this, 'treweler_token_admin_notices') 			);
    add_action( 'wp_ajax_treweler_get_admin_token', 			 array($this, 'treweler_get_admin_token')     			);
    add_action( 'admin_enqueue_scripts',            			 array($this, 'load_admin_style_and_scripts') 			);
	add_action( 'wp_loaded',                        			 array($this, 'treweler_fs_buffer_start')     			);
	add_action( 'shutdown',                         			 array($this, 'treweler_fs_buffer_end')       			);
	add_action( 'init',                             			 array($this, 'treweler_plugin_shortcode_add')			);
	add_action( 'wp_ajax_treweler_add_colorpicker_custom_color', array($this, 'treweler_add_colorpicker_custom_color')  );

    add_filter( 'upload_mimes', [ $this, 'add_new_mime_types' ] );
    add_filter( 'wp_check_filetype_and_ext', [ $this, 'add_allow_upload_extension_exception' ], 10, 5 );
  }

  /**
   * Add new mime types for media uploads
   *
   * @param $mime_types
   *
   * @return mixed
   */
  public function add_new_mime_types( $mime_types ) {
    $mime_types['svg'] = 'image/svg+xml';
    $mime_types['gpx'] = 'application/gpx+xml';

    return $mime_types;
  }

  /**
   * Exception for WordPress 4.7.1 file contents check system using finfo_file (wp-includes/functions.php)
   * In case of custom extension in this plugins' setting, the WordPress 4.7.1 file contents check system is always true.
   *
   * @param $data
   * @param $file
   * @param $filename
   * @param $mimes
   * @param $real_mime
   *
   * @return array
   */
  public function add_allow_upload_extension_exception( $data, $file, $filename, $mimes, $real_mime ) {
    $mime_type_values = false;

    $settings = [
      'mime_type_values'          => 'a:1:{i:0;s:25:"gpx = application/gpx+xml";}',
      'security_attempt_enable'   => 'no',
      'filename_sanitized_enable' => 'no'
    ];


    if ( ! isset( $settings['mime_type_values'] ) || empty( $settings['mime_type_values'] ) ) {
      return compact( 'ext', 'type', 'proper_filename' );
    } else {
      $mime_type_values = unserialize( $settings['mime_type_values'] );
    }

    $ext = $type = $proper_filename = false;
    if ( isset( $data['ext'] ) ) {
      $ext = $data['ext'];
    }
    if ( isset( $data['type'] ) ) {
      $ext = $data['type'];
    }
    if ( isset( $data['proper_filename'] ) ) {
      $ext = $data['proper_filename'];
    }
    if ( $ext != false && $type != false ) {
      return $data;
    }

    // If file extension is 2 or more
    $f_sp = explode( ".", $filename );
    $f_exp_count = count( $f_sp );

    // Filename type is "XXX" (There is not file extension).
    if ( $f_exp_count <= 1 ) {
      return $data;
      /* Even if the file extension is "XXX.ZZZ", "XXX.YYY.ZZZ", "AAA.XXX.YYY.ZZZ" or more, it always picks up  the tail of the extensions.
    */
    } else {
      $f_name = $f_sp[0];
      $f_ext = $f_sp[ $f_exp_count - 1 ];
      // WordPress sanitizes the filename in case of 2 or more extensions.
      // ex. XXX.YYY.ZZZ --> XXX_.YYY.ZZZ.
      // The following function fixes the sanitized extension when a file is uploaded in the media in case of allowed extensions.
      // ex. XXX.YYY.ZZZ -- sanitized --> XXX_.YYY.ZZZ -- fixed the plugin --> XXX.YYY.ZZZ
      // In detail, please see sanitize_file_name function in "wp-includes/formatting.php".
      //var_dump($settings['filename_sanitized_enable']);
      if ( isset( $settings['filename_sanitized_enable'] ) && $settings['filename_sanitized_enable'] === "yes" ) {
      } else {
        add_filter( 'sanitize_file_name', function($filename, $filename_raw){
          return str_replace("_.", ".", $filename);
        }, 10, 2 );
      }
    }

    // If "security_attempt_enable" option disables (default) in the admin menu, the plugin avoids the security check regarding a file extension by WordPress core because of flexible management.
    if ( isset( $settings['security_attempt_enable'] ) && $settings['security_attempt_enable'] === "yes" ) {
      return $data;
    }

    $flag = false;
    if ( ! empty( $mime_type_values ) ) {
      foreach ( (array) $mime_type_values as $line ) {
        // Ignore to the right of '#' on a line.
        $line = substr( $line, 0, strcspn( $line, '#' ) );
        // Escape Strings
        $line = wp_strip_all_tags( $line );

        $line_value = explode( "=", $line );
        if ( count( $line_value ) != 2 ) {
          continue;
        }
        // "　" is the Japanese multi-byte space. If the character is found out, it automatically change the space.
        if ( trim( $line_value[0] ) === $f_ext ) {
          $ext = $f_ext;
          $type = trim( str_replace( "　", " ", $line_value[1] ) );
          $flag = true;
          break;
        }
      }
    }

    if ( $flag ) {
      return compact( 'ext', 'type', 'proper_filename' );
    } else {
      return $data;
    }
  }

  public function treweler_plugin_main_menu() {
    add_submenu_page('edit.php?post_type=map', 'Settings', 'Settings', 'manage_options', 'treweler-settings', array($this,'treweler_sub_menu_settings'));
  }

  public function treweler_sub_menu_settings() {
    require TREWELER_PLUGIN_DIR . 'templates/admin/settings-template.php';
  }

  public function treweler_register_settings() {
    register_setting('treweler_mapbox_access_token', 'treweler_mapbox_access_token', array($this, 'treweler_token_validate'));
  }

  public function treweler_token_validate($args) {
    $isValid = wp_remote_get( "https://api.mapbox.com/geocoding/v5/mapbox.places/Los%20Angeles.json?access_token=". $args );
	if($isValid['response']['code'] != 200) {
 	  $args = '';
      add_settings_error('treweler_mapbox_access_token', 'treweler_invalid_token', 'Enter a valid Mapbox access token.', $type = 'error');
    }
    return $args;
  }

  public function treweler_token_admin_notices() {
    settings_errors();
  }

  public function treweler_get_admin_token() {
    echo base64_encode("###". get_option('treweler_mapbox_access_token') . "###"); exit;
  }

  public function load_admin_style_and_scripts() {
    if(get_post_type() == TREWELER_PLUGIN_CPT_MAP || get_post_type() == TREWELER_PLUGIN_CPT_MARKER || get_post_type() == TREWELER_PLUGIN_CPT_ROUTE ) {

		wp_enqueue_style( 'treweler-mapbox-style', '//api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.css', false, TREWELER_VERSION );
		wp_enqueue_style( 'treweler-mapbox-geocoder-style', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css', false, TREWELER_VERSION );
		wp_enqueue_style( 'treweler-style', TREWELER_PLUGIN_URL . 'assets/css/styles.css', false, TREWELER_VERSION );

		wp_enqueue_script( 'treweler-mapbox-script', '//api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.js', [], TREWELER_VERSION );
		wp_enqueue_script( 'treweler-mapbox-geocoder-script', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js', [], TREWELER_VERSION );
		wp_enqueue_script( 'treweler-marker-color', '//cdn.jsdelivr.net/npm/a-color-picker@1.1.8/dist/acolorpicker.js', [], TREWELER_VERSION );

		if(get_post_type() == TREWELER_PLUGIN_CPT_MAP) {
			wp_enqueue_script( 'treweler-script', TREWELER_PLUGIN_URL . 'assets/js/scripts.js', [], TREWELER_VERSION );
		} else if(get_post_type() == TREWELER_PLUGIN_CPT_MARKER) {
		    wp_enqueue_script( 'treweler-script', TREWELER_PLUGIN_URL . 'assets/js/marker-script.js', [], TREWELER_VERSION );
		} else if(get_post_type() == TREWELER_PLUGIN_CPT_ROUTE) {
		//	wp_enqueue_style( 'treweler-mapbox-direction-style', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.css', false, TREWELER_VERSION );
			wp_enqueue_style( 'treweler-mapbox-draw-style', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css', false, TREWELER_VERSION );

		//	wp_enqueue_script( 'treweler-mapbox-direction', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-directions/v4.0.2/mapbox-gl-directions.js', [], TREWELER_VERSION );
			wp_enqueue_script( 'treweler-mapbox-draw', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js', [], TREWELER_VERSION );
			wp_enqueue_script( 'treweler-script', TREWELER_PLUGIN_URL . 'assets/js/route-script.js', [], TREWELER_VERSION );
		}

    } else if(get_post_type() == 'page') {

		wp_enqueue_style( 'treweler-style', TREWELER_PLUGIN_URL . 'assets/css/styles.css', false, TREWELER_VERSION );

	}
    wp_localize_script( 'treweler-script', 'obj', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  }

  /**
   * Buffer the page content and replace with fullscreen-map
   * It's perform action on `wp_loaded` and render fullscreen-map instead of page content.
   */
  public function treweler_fs_buffer_start() {
    ob_start( array($this, 'treweler_fs_buffer_filter') );
  }

  public function treweler_fs_buffer_filter( $pageBuffer ) {
	$jsonCheck = json_decode($pageBuffer);
    if(!is_admin() && json_last_error()!=0) {
	  $fs_meta = get_post_meta( get_the_ID(), '_treweler_cpt_dd_box_fullscreen', true);

	  if( trim($fs_meta)!=="" && $fs_meta > 0 ) {
		$post = get_post_meta( $fs_meta );
		$markers = self::getMarkersOfMap($fs_meta);
		/***
		$str = '';
		foreach($markers as $m) {
			$str .= join(", ", $m['latlng']) . "<br/>";
		}
		return $str;
		***/
		$routes = self::getRoutesOfMap($fs_meta);

		$latlng = unserialize($post['_treweler_map_latlng'][0]);
		if(is_array($latlng) && !empty($latlng)) {
		  foreach($latlng as  $k => $v) {
		  	${"ll$k"} = $v;
		  }
		} else {
			$ll0 = 40.730610;$ll1 = -73.935242;
		}
		$style = $post['_treweler_map_styles'][0];
		$customStyle = $post['_treweler_map_custom_style'][0];
		$maxZoom = trim($post['_treweler_map_max_zoom'][0])!="" ? $post['_treweler_map_max_zoom'][0] : 24;
		$minZoom = trim($post['_treweler_map_min_zoom'][0])!="" ? $post['_treweler_map_min_zoom'][0] : 0;
		$zoom = trim($post['_treweler_map_zoom'][0])!="" ? $post['_treweler_map_zoom'][0] : 7;
		$controls = $post['_treweler_map_controls'][0];
		$clusterColor = $post['_treweler_map_cluster_color'][0];
		if(trim($controls)!=""){
			foreach( unserialize($controls) as  $k => $v) {
				${"chk$k"} = $v;
			}
		}

		$fs_map = "<html lang='en'>
  <head>
	<title>". get_the_title() ." | ".  get_bloginfo() ."</title>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<meta name='description' content='' />
	<meta name='keywords' content='' />
	<link rel='stylesheet' href='//api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.css' media='all' />
	<link rel='stylesheet' href='//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css' media='all' />
	<link rel='stylesheet' href='//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css' media='all' />
	<link rel='stylesheet' href='". TREWELER_PLUGIN_URL ."assets/css/treweler-style.css' />
  </head>
  <body>
	<div id='map'></div>";

	if($post['_treweler_map_details_name'][0] != "" || $post['_treweler_map_details_description'][0] != "" || get_the_post_thumbnail_url($fs_meta) != ""){
	$fs_map .= '<div class="treweler-map-details logo '. $post['_treweler_map_details_position'][0] .'">
					<div class="logo-tw">';
				if(get_the_post_thumbnail_url($fs_meta) != ""){
				$fs_map .= '<div class="logo-icon">
					<img src="'. get_the_post_thumbnail_url($fs_meta) .'" alt="Treweler">
				</div>';
				}
				if($post['_treweler_map_details_name'][0] != "" || $post['_treweler_map_details_description'][0] != ""){
				$fs_map .= '<div class="logo-text">';
					if($post['_treweler_map_details_name'][0] != ""){
					$fs_map .= '<h3 style="color:'. $post['_treweler_map_details_name_color'][0] .';">'. trim($post['_treweler_map_details_name'][0]) .'</h3>';
					}
					if($post['_treweler_map_details_description'][0] != ""){
					$fs_map .= '<span style="color:'. $post['_treweler_map_details_description_color'][0] .';">'. trim($post['_treweler_map_details_description'][0]) .'</span>';
					}
				$fs_map .= '</div>';
				}
	$fs_map .= '	</div>
				</div>';
	}

	$fs_map .= "
	<script src='//code.jquery.com/jquery-2.2.4.min.js'></script>
	<script src='//api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.js'></script>
	<script src='//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js'></script>
	<script src='//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js'></script>
    <script>
	mapboxgl.accessToken = '". get_option('treweler_mapbox_access_token') ."';
    map = new mapboxgl.Map({
      container: 'map',
      style: '". (trim($customStyle) != '' ? $customStyle : $style) ."',
      center: [$ll1, $ll0],
	  minZoom: $minZoom,
	  maxZoom: $maxZoom,
      zoom: $zoom
    });";
	if(isset($chk0)){
	$fs_map .= "
	  map.addControl(new mapboxgl.ScaleControl({
        maxWidth: 100,
        unit: 'imperial'
      }), 'bottom-right');";
	}
	if(isset($chk1)){
	$fs_map .= "map.addControl(new mapboxgl.FullscreenControl());";
	}
	if(isset($chk2)){
	$fs_map .= "
	  map.addControl(
        new MapboxGeocoder({
          accessToken: mapboxgl.accessToken,
          zoom: 14,
          placeholder: 'Enter search...',
          mapboxgl: mapboxgl,
		  marker: false
        }), 'top-left'
      );";
	}
	if(isset($chk3)){
	$fs_map .= "map.addControl(new mapboxgl.NavigationControl());";
	}

	/** Add Marker & Cluster **/
	$fs_map .= "
		var clusterData = {
			'type': 'FeatureCollection',
			'features': [";

			if(is_array($markers) && !empty($markers)){
				foreach($markers as $marker){
				  if($marker['icon'] == "" ) {
					$fs_map .= "
					{
						'type': 'Feature',
						'geometry': {
							'type': 'Point',
							'coordinates': [". $marker['latlng'][1] .", ". $marker['latlng'][0] ."]
						 },
						 'properties': {
							'id': ". $marker['id'] .",
							'color': '". $marker['color'] ."',
							'type': '". $marker['style'] ."'
						 }
					},
					";
				  } else {
					$fs_map .= "
					{
						'type': 'Feature',
						'geometry': {
							'type': 'Point',
							'coordinates': [". $marker['latlng'][1] .", ". $marker['latlng'][0] ."]
						 },
						 'properties': {
							'id': ". $marker['id'] .",
							'icon': '". $marker['icon'] ."',
							'anchor': '". $marker['anchor'] ."',
							'size': '". $marker['size'] ."'
						 }
					},
					";
				  }
				}
			}

	$fs_map .= "]
		}";

	/**
	 *

	if(is_array($markers) && !empty($markers)){
	  foreach($markers as $marker){
		if($marker['style'] === "dark") {
		$fs_map .= "
			var markerEle = document.createElement('div');
			markerEle.className = 'treweler-marker';
			markerEle.innerHTML = '<div class=\"marker marker--dark\"><div class=\"marker-wrap\"><div class=\"marker__shadow\"><div class=\"marker__border\" style=\"border-color:". $marker['color'] .";\"><div class=\"marker__center\"></div></div></div></div></div>';
		    marker = new mapboxgl.Marker(markerEle).setLngLat([".$marker['latlng'][1] .", ".$marker['latlng'][0] ."]).addTo(map);";
		} else if($marker['style'] === "light") {
		$fs_map .= "
			var markerEle = document.createElement('div');
			markerEle.className = 'treweler-marker';
			markerEle.innerHTML = '<div class=\"marker\"><div class=\"marker-wrap\"><div class=\"marker__shadow\"><div class=\"marker__border\" style=\"border-color:". $marker['color'] .";\"><div class=\"marker__center\"></div></div></div></div></div>';
		    marker = new mapboxgl.Marker(markerEle).setLngLat([".$marker['latlng'][1] .", ".$marker['latlng'][0] ."]).addTo(map);";
		}
	  }
	}

	 *
	 */


	$fs_map .= "
		map.on('load', function() {
			// add a clustered GeoJSON source for a sample set of earthquakes
			map.addSource('locations', {
				type: 'geojson',
				data: clusterData,
				cluster: true,
				clusterMaxZoom: 14,
				clusterRadius: 50,
			});

			map.addLayer({
				'id': 'clusters',
				'type': 'symbol',
				'source': 'locations',
				'filter': ['!=', 'cluster', true]
			});

			map.addLayer({
				'id': 'markerPnt',
				'type': 'symbol',
				'source': 'locations',
				'filter': ['!=', 'cluster', true],
				'layout': {
					'text-field': [
						'number-format',
						['get', 'mag'],
						{ 'min-fraction-digits': 1, 'max-fraction-digits': 1 }
					],
					'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
					'text-size': 10
				}
			});

			// objects for caching and keeping track of HTML marker objects (for performance)
			var markers = {};
			var markersOnScreen = {};
 
			function updateMarkers() {
				var newMarkers = {};
				var features = map.querySourceFeatures('locations');
				// for every cluster on the screen, create an HTML marker for it (if we didn't yet),
				// and add it to the map if it's not there already
				for (var i = 0; i < features.length; i++) {
					var coords = features[i].geometry.coordinates;
					var props = features[i].properties;
					if (!props.cluster) {
						var id = 1000 + props.id;
						var marker = markers[id];
						if (!marker) { 

						/* 	var el = createNewMarker(props);
							marker = markers[id] = new mapboxgl.Marker({
													element: el
												}).setLngLat(coords); */

							var elObj = createNewMarker(props);
							marker = markers[id] = new mapboxgl.Marker(elObj).setLngLat(coords);
						}
					} else {
						var id = props.cluster_id;
						var marker = markers[id];
						if (!marker) { 
							var el = createNewCluster(props);
								el.addEventListener('click', (e) => {
								//	var feats = map.queryRenderedFeatures({ layers: ['clusters'] });

									if(!features[0]) return;
									var clusterId = features[0].properties.cluster_id;
									map.getSource('locations').getClusterExpansionZoom(clusterId, function (err, zoom) {
										if (err)
											return;

										map.flyTo({
											center: coordinates[el.cluster_id],
											zoom: zoom,
											speed: .75
										});
									});
								});
								
							marker = markers[id] = new mapboxgl.Marker({
													element: el
												}).setLngLat(coords);
						}
					}
					newMarkers[id] = marker;
		 
					if (!markersOnScreen[id]) marker.addTo(map);
				}
			
				// for every marker we've added previously, remove those that are no longer visible
				for (id in markersOnScreen) {
					if (!newMarkers[id]) markersOnScreen[id].remove();
				}
				markersOnScreen = newMarkers;
			}
 
			// after the GeoJSON data is loaded, update markers on the screen and do so on every map move/moveend
			map.on('data', function(e) {
				if (e.sourceId !== 'locations' || !e.isSourceLoaded) return;

				map.on('move', updateMarkers);
				map.on('moveend', updateMarkers);
				updateMarkers();
			});
			";

			if(is_array($routes) && !empty($routes)){
				foreach($routes as $route){
				  if($route['routeProfile'] != 'no' && trim($route['routeGPX']) == "" ) {

					$fs_map .= " 
					var plotRoute;
					jQuery.ajax({
						type: 'GET',
						url: 'https://api.mapbox.com/directions/v5/mapbox/". $route['routeProfile'] ."/". $route['routeCoords'] ."?geometries=geojson&access_token=". get_option('treweler_mapbox_access_token') ."'
					}).done( function(data) {
					  randNo = Math.ceil(Math.random() * Math.random() * 100);
					  plotRoute = data.routes[0].geometry;
					  map.addSource('route'+randNo, {
					  	'type': 'geojson',
					  	'data': {
					  		'type': 'Feature',
					  		'properties': {},
					  		'geometry': plotRoute
					  	}
					  });
					  map.addLayer({
					  	'id': 'route'+randNo,
					  	'type': 'line',
					  	'source': 'route'+randNo,
					  	'layout': {
					  		'line-join': 'round',
					  		'line-cap': 'round'
					  	},
					  	'paint': {
					  		'line-color':     '". $route['routeColor'] ."',
					  		'line-width':     ". $route['routeLineWidth'] .",
							'line-opacity':   ". $route['routeLineOpacity'] .",
							'line-dasharray': [". $route['routeLineDash'] .",". $route['routeLineGap'] ."]
					  	}
					  });
					});
					";

				  } else {
					if(trim($route['routeGPX'])=="") {

						$fs_coords = [];
						$coords_str = explode(";", $route['routeCoords']);
						foreach($coords_str as $lls) {
							$ll = explode(",", $lls);
							$llA = [];
							foreach($ll as $l) {
								$llA[] = floatval($l);
							}
							$fs_coords[] = $llA;
						}

							$fs_map .= " 
							  var plotRoute = ". json_encode($fs_coords) .";
							  randNo = Math.ceil(Math.random() * Math.random() * 100);
							  map.addSource('route'+randNo, {
								'type': 'geojson',
								'data': {
									'type': 'Feature',
									'properties': {},
									'geometry': {
										'type': 'LineString',
										'coordinates': plotRoute
									}
								}
							  });
							  map.addLayer({
								'id': 'route'+randNo,
								'type': 'line',
								'source': 'route'+randNo,
								'layout': {
									'line-join': 'round',
									'line-cap': 'round'
								},
								'paint': {
									'line-color':     '". $route['routeColor'] ."',
									'line-width':     ". $route['routeLineWidth'] .",
									'line-opacity':   ". $route['routeLineOpacity'] .",
									'line-dasharray': [". $route['routeLineDash'] .",". $route['routeLineGap'] ."]
								}
							  });
							";
					} else {
						$fs_map .= "
							jQuery.ajax({
								type: 'GET',
								url: '". $route['routeGPX'] ."',
								dataType: 'xml',
								success: function(GPX){
									var points = [];
									jQuery(GPX).find('trkpt').each(function () {
										points.push([parseFloat(jQuery(this).attr('lon')), parseFloat(jQuery(this).attr('lat'))]);
									});
									if(points.length) {
										  randNo = Math.ceil(Math.random() * Math.random() * 100);
										  map.addSource('route'+randNo, {
											'type': 'geojson',
											'data': {
												'type': 'Feature',
												'properties': {},
												'geometry': {
													'type': 'LineString',
													'coordinates': points
												}
											}
										  });
										  map.addLayer({
											'id': 'route'+randNo,
											'type': 'line',
											'source': 'route'+randNo,
											'layout': {
												'line-join': 'round',
												'line-cap': 'round'
											},
											'paint': {
												'line-color':     '". $route['routeColor'] ."',
												'line-width':     ". $route['routeLineWidth'] .",
												'line-opacity':   ". $route['routeLineOpacity'] .",
												'line-dasharray': [". $route['routeLineDash'] .",". $route['routeLineGap'] ."]
											}
										  });
										
									}
								}
							})
						";
					}

				  }
				}
			}

			$fs_map .= "

		});

		// HTML Cluster Element
		function createNewCluster(props) {

			var html = '<div class=\"treweler-marker-cluster\"><div class=\"marker marker--cluster\" >'
					+'<div class=\"marker-wrap\">'
						+'<div class=\"marker__shadow\" style=\"border-color:'+ hex2rgba('". $clusterColor ."', 0.1) +'\">'
							+'<div class=\"marker__border\" style=\"border-color:'+ hex2rgba('". $clusterColor ."', 0.4) +'\">'
								+'<div class=\"marker__center\" style=\"background-color:". $clusterColor .";\">'+ props.point_count +'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div></div>';

			var el = document.createElement('div');
			el.innerHTML = html;

			return el.firstChild;
		}

		// HTML Marker Element
		function createNewMarker(props) {
			if(props.icon == '' || props.icon == undefined) {

				let isDark = (props.type=='dark' ? 'marker--dark':'');
				var html = '<div class=\"treweler-marker\"><div class=\"marker '+ isDark +'\">'
					+'<div class=\"marker-wrap\">'
						+'<div class=\"marker__shadow\">'
							+'<div class=\"marker__border\"  style=\"border-color:'+ props.color +';\">'
								+'<div class=\"marker__center\"></div>'
							+'</div>'
						+'</div>'
					+'</div>'
				+'</div></div>';

				var el = document.createElement('div');
				el.innerHTML = html;
				
				//	return el.firstChild;

				let elementObj = {
							element: el,
							anchor: 'center'
					}
				return elementObj;

			} else {

			//	var html = '<div class=\"treweler-marker\" style=\"background-image:url('+ props.icon +');background-size:contain;background-repeat:no-repeat;background-position: center top;height:42px;width:42px;top:-21px;left:0px;\"></div>';
			//	var el = document.createElement('div');
			//	el.innerHTML = html;
			//	return el.firstChild;

				let size = props.size!='' ? props.size.split(';') : '42;42'.split(';');
				var el = document.createElement('div');
			    el.className = 'treweler-marker icon';
				el.style.backgroundImage = 'url('+props.icon+')';
				el.style.backgroundSize = 'contain';
				el.style.backgroundRepeat = 'no-repeat';
				el.style.backgroundPosition = 'center top';
				el.style.width = (parseInt(size[1])<=42 ? (parseInt(size[1])%2==0 ? parseInt(size[1]) : parseInt(size[1])+1) : 42 )+'px';
				el.style.height = (parseInt(size[0])<=42 ? (parseInt(size[0])%2==0 ? parseInt(size[0]) : parseInt(size[0])+1) : 42 )+'px';

				let elementObj = {
						element: el,
						anchor: props.anchor,
					/*	offset:[-size[1]/2, -size[0]] */
					}
				return elementObj;

			}
		
		}
		
		//Hex to RGBA color
		function hex2rgba(hex,opacity){
			hex = hex.replace('#','');
			r = parseInt(hex.substring(0,2), 16);
			g = parseInt(hex.substring(2,4), 16);
			b = parseInt(hex.substring(4,6), 16);

			return 'rgba('+r+','+g+','+b+','+opacity+')';
		}
		
		// inspect a cluster on click
		map.on('click', 'clusters', function(e) {
			var features = map.queryRenderedFeatures(e.point, {
				layers: ['clusters']
			});
			var clusterId = features[0].properties.cluster_id;
			map.getSource('earthquakes').getClusterExpansionZoom(
				clusterId,
				function(err, zoom) {
					if (err) return;

					map.easeTo({
						center: features[0].geometry.coordinates,
						zoom: zoom
					});
				}
			);
		});
		
		map.on('mouseenter', 'clusters', function () {
			map.getCanvas().style.cursor = 'pointer';
		});
		map.on('mouseleave', 'clusters', function () {
			map.getCanvas().style.cursor = '';
		});
	</script>
  </body>
</html>";

		$pageBuffer = $fs_map;
	  }
	  return $pageBuffer;
    } else {
      return $pageBuffer;
    }
  }

  public function treweler_fs_buffer_end() {
    if ( ob_get_length() ) {
      ob_end_flush();
    }
  }

  /**
   * Get Markers for particular map
   */
  public function getMarkersOfMap($mapId="") {

	  $args = array(
	  	// basics
	  	'post_type'   => 'marker',
	  	'post_status' => 'publish',
		'posts_per_page' => '-1',
	  	// meta query
	  	'meta_query' => array(
	  	  'relation' => 'AND',
	  	  array(
	  		'key'     => '_treweler_marker_map_id',
	  		'value'   => $mapId,
	  		'compare' => '='
	  	  )
	  	)
	  );
	  $getMarkers = new WP_Query( $args );
	  if($getMarkers->post_count > 0) {
		  $c = 0;
		  foreach($getMarkers->posts as $k => $v) {
			$markers[$c]['id'] = $v->ID;
			$markers[$c]['style'] = get_post_meta($v->ID, '_treweler_marker_style', true);
			$markers[$c]['color'] = get_post_meta($v->ID, '_treweler_marker_color', true);
			$markers[$c]['latlng'] = get_post_meta($v->ID, '_treweler_marker_latlng', true);
			$markers[$c]['icon'] = get_the_post_thumbnail_url($v->ID);
			$markers[$c]['anchor'] = get_post_meta($v->ID, '_treweler_marker_position', true);
			$markers[$c]['size'] = get_post_meta($v->ID, '_treweler_marker_icon_size', true);
			$c++;
		  }
		  return $markers;
	  }
	  return array();
  }

  /**
   * Get Routes for particular map
   */
  public function getRoutesOfMap($mapId="") {

	  $args = array(
	  	// basics
	  	'post_type'   => 'route',
	  	'post_status' => 'publish',
		'posts_per_page' => '-1',
	  	// meta query
	  	'meta_query' => array(
	  	  'relation' => 'AND',
	  	  array(
	  		'key'     => '_treweler_route_map_id',
	  		'value'   => $mapId,
	  		'compare' => '='
	  	  )
	  	)
	  );
	  $getRoutes = new WP_Query( $args );
	  if($getRoutes->post_count > 0) {
		  $c = 0;
		  foreach($getRoutes->posts as $k => $v) {
			$routes[$c]['id'] = $v->ID;
			$routes[$c]['routeCoords'] = get_post_meta($v->ID, '_treweler_route_line_coords', true);
			$routes[$c]['routeGPX'] = get_post_meta($v->ID, '_treweler_route_gpx_file', true);;
			$routes[$c]['routeColor'] = get_post_meta($v->ID, '_treweler_route_line_color', true);
			$routes[$c]['routeProfile'] = get_post_meta($v->ID, '_treweler_route_profile', true);
			$routes[$c]['routeLineWidth'] = get_post_meta($v->ID, '_treweler_route_line_width', true);
			$routes[$c]['routeLineOpacity'] = get_post_meta($v->ID, '_treweler_route_line_opacity', true);
			$routes[$c]['routeLineDash'] = get_post_meta($v->ID, '_treweler_route_line_dash', true);
			$routes[$c]['routeLineGap'] = get_post_meta($v->ID, '_treweler_route_line_gap', true);
			$c++;
		  }
		  return $routes;
	  }
	  return array();
  }
  /**
   * Enqueue style & script for front-side shortcode.
   */
  public function treweler_load_front_style_and_scripts() {
    wp_enqueue_style( 'treweler-mapbox-style', '//api.mapbox.com/mapbox-gl-js/v1.7.0/mapbox-gl.css', false, TREWELER_VERSION );
    wp_enqueue_style( 'treweler-mapbox-geocoder-style', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.css', false, TREWELER_VERSION );
    wp_enqueue_style( 'treweler-mapbox-draw-style', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css', false, TREWELER_VERSION );
	wp_enqueue_style( 'treweler-style', TREWELER_PLUGIN_URL . 'assets/css/treweler-style.css', false, TREWELER_VERSION );

    wp_enqueue_script( 'treweler-mapbox-script', '//api.mapbox.com/mapbox-gl-js/v1.8.0/mapbox-gl.js', [], TREWELER_VERSION );
    wp_enqueue_script( 'treweler-mapbox-geocoder-script', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.4.2/mapbox-gl-geocoder.min.js', [], TREWELER_VERSION );
    wp_enqueue_script( 'treweler-mapbox-draw', '//api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js', [], TREWELER_VERSION );
	wp_enqueue_script( 'treweler-mapbox-js',  TREWELER_PLUGIN_URL . 'assets/js/treweler-shortcode.js', [], TREWELER_VERSION );
  }

  /**
   * Shortcode for treweler plugin
   * `[treweler map-id='1']`
   * Attributes:
   * - map-id: (*required) Map ID to choose map to show in front-side.
   * - height: Treweler map height. default, 100px;
   * - width: Treweler map width. default, 100%;
   * - type: `fullwidth`, used for fullwidth map outside container and blocks. default value `null`.
   */
  public function treweler_plugin_shortcode( $attr = [], $content = null, $tag = '' ) {
	if($tag == TREWELER_PLUGIN_SHORTCODE_TAG && isset($attr['map-id']) && $attr['map-id']!=0 && trim($attr['map-id'])!="" ) {
		// normalize attribute keys, lowercase
		$attr = array_change_key_case((array)$attr, CASE_LOWER);
		if(!is_admin() && get_post_status($attr['map-id']) == "publish" && get_post_type($attr['map-id']) == TREWELER_PLUGIN_CPT_MAP ) {
			//Load mapbox style & script library
			self::treweler_load_front_style_and_scripts();
			$height = isset($attr['height']) ? (trim($attr['height'])!=""? $attr['height']:'100px') : '';
			$height = ( $height!=""?"height:".$height.";" : "" );
			$width = isset($attr['width']) ? (trim($attr['width'])!=""? $attr['width']:'100%') : '';
			$width = ( $width!=""?"width:". $width.";" : "" );
			$typeFW = (isset($attr['type']) && $attr['type'] == "fullwidth") ? 'treweler-map-fw' : '';
			if(isset($attr['scrollzoom'])) {
				if($attr['scrollzoom'] == 'no'){
					$scrollzoomActive = false;
				} else {
					$scrollzoomActive = true;
				}
			} else {
				$scrollzoomActive = true;
			}
			//Generate random number for map initialization ID.
			$randId = rand(10,10000);
			$mapID = "map-". $attr['map-id'] ."-". $randId;
			$markers = self::getMarkersOfMap($attr['map-id']);
			$routes = self::getRoutesOfMap($attr['map-id']);
			$scop = '<div class="map-wrapper '. $typeFW .'">
						<div id="'. $mapID .'" class="treweler-map treweler-map-'. $attr['map-id'] .'" style="'. $height . $width .'">';

							/* MAP DATA - POSTMETA */
							$mapData = get_post_meta( $attr['map-id'] );
							if( (isset($mapData['_treweler_map_details_name']) && $mapData['_treweler_map_details_name'][0] != "") ||
								(isset($mapData['_treweler_map_details_description']) && $mapData['_treweler_map_details_description'][0] != "") ||
								get_the_post_thumbnail_url($attr['map-id']) != "" ) {
							$scop .= '<div class="shortcode-map-content">
										<div class="treweler-map-details logo '. $mapData['_treweler_map_details_position'][0] .'">
											<div class="logo-tw">';
										if(get_the_post_thumbnail_url($attr['map-id']) != ""){
											$scop .= '<div class="logo-icon"><img src="'. get_the_post_thumbnail_url($attr['map-id']) .'" alt="Treweler"></div>';
										}
										if($mapData['_treweler_map_details_name'][0] != "" || $mapData['_treweler_map_details_description'][0] != ""){
											$scop .= '<div class="logo-text">';
											if($mapData['_treweler_map_details_name'][0] != ""){
												$scop .= '<h3 style="color:'. $mapData['_treweler_map_details_name_color'][0] .';">'. trim($mapData['_treweler_map_details_name'][0]) .'</h3>';
											}
											if($mapData['_treweler_map_details_description'][0] != ""){
												$scop .= '<span style="color:'. $mapData['_treweler_map_details_description_color'][0] .';">'. trim($mapData['_treweler_map_details_description'][0]) .'</span>';
											}
											$scop .= '</div>';
										}
							$scop .= '		</div>
										</div>
									  </div>';
							}
							/* MAP DATA - POSTMETA */

			$scop .= '	</div>
					 </div>';

			$latlng = unserialize($mapData['_treweler_map_latlng'][0]);
			if(is_array($latlng) && !empty($latlng)) {
			  foreach($latlng as  $k => $v) {
				${"ll$k"} = $v;
			  }
			} else {
				$ll0 = 40.730610;$ll1 = -73.935242;
			}
			$style = $mapData['_treweler_map_styles'][0];
			$customStyle = $mapData['_treweler_map_custom_style'][0];
			$maxZoom = trim($mapData['_treweler_map_max_zoom'][0])!="" ? $mapData['_treweler_map_max_zoom'][0] : 24;
			$minZoom = trim($mapData['_treweler_map_min_zoom'][0])!="" ? $mapData['_treweler_map_min_zoom'][0] : 0;
			$zoom = trim($mapData['_treweler_map_zoom'][0])!="" ? $mapData['_treweler_map_zoom'][0] : 7;
			$controls = $mapData['_treweler_map_controls'][0];
			$clusterColor = $mapData['_treweler_map_cluster_color'][0];
			if(trim($controls)!=""){
				foreach( unserialize($controls) as  $k => $v) {
					${"chk$k"} = $v;
				}
			}

			/* MAP DATA - POSTMETA */

			$scopScript = ' mapboxgl.accessToken = "'. get_option('treweler_mapbox_access_token') .'";
			  map_'. str_replace("-", "_", $mapID) .'  = new mapboxgl.Map({
			    container: "'. $mapID .'",
			    style: "'. (trim($customStyle) != '' ? $customStyle : $style) .'",
			    center: ['.$ll1.', '.$ll0.'],
				minZoom: '.$minZoom.',
				maxZoom: '.$maxZoom.',
				zoom: '.$zoom.'
			  });';

			if(isset($chk0)){
			$scopScript .= "
			  map_". str_replace("-", "_", $mapID).".addControl(new mapboxgl.ScaleControl({
				maxWidth: 100,
				unit: 'imperial'
			  }), 'bottom-right');";
			}
			if(isset($chk1)){
			$scopScript .= "map_". str_replace("-", "_", $mapID).".addControl(new mapboxgl.FullscreenControl());";
			}
			if(isset($chk2)){
			$scopScript .= "
			  map_". str_replace("-", "_", $mapID).".addControl(
				new MapboxGeocoder({
				  accessToken: mapboxgl.accessToken,
				  zoom: 14,
				  placeholder: 'Enter search...',
				  mapboxgl: mapboxgl,
				  marker: false
				}), 'top-left'
			  );";
			}
			if(isset($chk3)){
			$scopScript .= "map_". str_replace("-", "_", $mapID).".addControl(new mapboxgl.NavigationControl());";
			}

			if( $scrollzoomActive ) {
				$scopScript .= " map_". str_replace("-", "_", $mapID).".scrollZoom.enable();";
			} else {
				$scopScript .= " map_". str_replace("-", "_", $mapID).".scrollZoom.disable();";
			}

	/** Add Marker & Cluster **/

	$scopScript .= "
		var clusterData_". str_replace("-", "_", $mapID)." = {
			'type': 'FeatureCollection',
			'features': [";

			if(is_array($markers) && !empty($markers)){
				foreach($markers as $marker){
				  if($marker['icon'] == "" ) {
					$scopScript .= "
					{
						'type': 'Feature',
						'geometry': {
							'type': 'Point',
							'coordinates': [". $marker['latlng'][1] .", ". $marker['latlng'][0] ."]
						 },
						 'properties': {
							'id': ". $marker['id'] .",
							'color': '". $marker['color'] ."',
							'type': '". $marker['style'] ."'
						 }
					},
					";
				  } else {
					$scopScript .= "
					{
						'type': 'Feature',
						'geometry': {
							'type': 'Point',
							'coordinates': [". $marker['latlng'][1] .", ". $marker['latlng'][0] ."]
						},
						'properties': {
							'id': ". $marker['id'] .",
							'icon': '". $marker['icon'] ."',
							'size': '". $marker['size'] ."'
						}
					},
					";
				  }
				}
			}

	$scopScript .= "],
			'clusterColor': '". $clusterColor ."'
		}
	";

			/**
			 * Add Marker
			 *
			if(is_array($markers) && !empty($markers)){
			  foreach($markers as $marker){
				if($marker['style'] === "dark") {
				$scopScript .= "
					var markerEle = document.createElement('div');
					markerEle.className = 'treweler-marker';
					markerEle.innerHTML = '<div class=\"marker marker--dark\"><div class=\"marker-wrap\"><div class=\"marker__shadow\"><div class=\"marker__border\" style=\"border-color:". $marker['color'] .";\"><div class=\"marker__center\"></div></div></div></div></div>';
					marker = new mapboxgl.Marker(markerEle).setLngLat([".$marker['latlng'][1] .", ".$marker['latlng'][0] ."]).addTo(map);";
				} else if($marker['style'] === "light") {
				$scopScript .= "
					var markerEle = document.createElement('div');
					markerEle.className = 'treweler-marker';
					markerEle.innerHTML = '<div class=\"marker\"><div class=\"marker-wrap\"><div class=\"marker__shadow\"><div class=\"marker__border\" style=\"border-color:". $marker['color'] .";\"><div class=\"marker__center\"></div></div></div></div></div>';
					marker = new mapboxgl.Marker(markerEle).setLngLat([".$marker['latlng'][1] .", ".$marker['latlng'][0] ."]).addTo(map);";
				}
			  }
			}
			 *
			 */

			$scopScript .= "window.addEventListener('resize', function(){
				var mapElement = document.getElementById('". $mapID ."');
				let fixW = '". $width ."';
				if(fixW.indexOf('px') >= 0) {
					fixW = fixW.substring(6, fixW.length - 3);
					if( parseInt(fixW) > this.innerWidth ) {
					  mapElement.style.width = this.innerWidth +'px';
					} else {
					  mapElement.style.width = fixW +'px';
					}
				} else if(fixW.indexOf('em') >= 0) {
					fixW = fixW.substring(6, fixW.length - 3);
					if( parseInt(fixW) > parseInt(this.innerWidth/16) ) {
					  mapElement.style.width = parseInt(this.innerWidth/16) +'em';
					} else {
					  mapElement.style.width = fixW +'em';
					}
				}
			});
			map_". str_replace("-", "_", $mapID).".on('load', function(){
				var mapElement = document.getElementById('". $mapID ."');
				let fixW = '". $width ."';
				if(fixW.indexOf('px') >= 0) {
					fixW = fixW.substring(6, fixW.length - 3);
					if( parseInt(fixW) > window.screen.width ) {
					  mapElement.style.width = window.screen.width +'px';
					} else {
					  mapElement.style.width = fixW +'px';
					}
				} else if(fixW.indexOf('em') >= 0) {
					fixW = fixW.substring(6, fixW.length - 3);
					if( parseInt(fixW) > parseInt(window.screen.width/16) ) {
					  mapElement.style.width = parseInt(window.screen.width/16) +'em';
					} else {
					  mapElement.style.width = fixW +'em';
					}
				}
				window.dispatchEvent(new Event('resize'));
				
				/* Add Cluster Marker */
				map_". str_replace("-", "_", $mapID).".addSource('locations_". str_replace("-", "_", $mapID)."', {
					type: 'geojson',
					data: clusterData_". str_replace("-", "_", $mapID).",
					cluster: true,
					clusterMaxZoom: 14,
					clusterRadius: 50,
				});

				map_". str_replace("-", "_", $mapID).".addLayer({
					'id': 'clusters_". str_replace("-", "_", $mapID)."',
					'type': 'symbol',
					'source': 'locations_". str_replace("-", "_", $mapID)."',
					'filter': ['!=', 'cluster', true]
				});
			
				map_". str_replace("-", "_", $mapID).".addLayer({
					'id': 'markerPnt_". str_replace("-", "_", $mapID)."',
					'type': 'symbol',
					'source': 'locations_". str_replace("-", "_", $mapID)."',
					'filter': ['!=', 'cluster', true],
					'layout': {
						'text-field': [
							'number-format',
							['get', 'mag'],
							{ 'min-fraction-digits': 1, 'max-fraction-digits': 1 }
						],
						'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
						'text-size': 10
					}
				});
		 
				var markers_". str_replace("-", "_", $mapID)." = {};
				var markersOnScreen_". str_replace("-", "_", $mapID)." = {};
 
				function updateMarkers() {
					var newMarkers_". str_replace("-", "_", $mapID)." = {};
					var features = map_". str_replace("-", "_", $mapID).".querySourceFeatures('locations_". str_replace("-", "_", $mapID)."');
					for (var i = 0; i < features.length; i++) {
						var coords = features[i].geometry.coordinates;
						var props = features[i].properties;
						if (!props.cluster) {
							var id = 1000 + props.id;
							var marker = markers_". str_replace("-", "_", $mapID)."[id];
							if (!marker) { 
								var elObj = createNewMarker(props);
								/* marker = markers_". str_replace("-", "_", $mapID)."[id] = new mapboxgl.Marker({
														element: el
													}).setLngLat(coords); */
								marker = markers_". str_replace("-", "_", $mapID)."[id] = new mapboxgl.Marker(elObj).setLngLat(coords);

							}
						} else {
							var id = props.cluster_id;
							var marker = markers_". str_replace("-", "_", $mapID)."[id];
							if (!marker) {
								props['color'] = clusterData_". str_replace("-", "_", $mapID) .".clusterColor;
								var el = createNewCluster(props);
								marker = markers_". str_replace("-", "_", $mapID)."[id] = new mapboxgl.Marker({
														element: el
													}).setLngLat(coords);
							}
						}

						newMarkers_". str_replace("-", "_", $mapID)."[id] = marker;

						if (!markersOnScreen_". str_replace("-", "_", $mapID)."[id]) marker.addTo(map_". str_replace("-", "_", $mapID).");
					}

					for (id in markersOnScreen_". str_replace("-", "_", $mapID).") {
						if (!newMarkers_". str_replace("-", "_", $mapID)."[id]) markersOnScreen_". str_replace("-", "_", $mapID)."[id].remove();
					}
					markersOnScreen_". str_replace("-", "_", $mapID)." = newMarkers_". str_replace("-", "_", $mapID).";
				}

				map_". str_replace("-", "_", $mapID).".on('data', function(e) {
					if (e.sourceId !== 'locations_". str_replace("-", "_", $mapID)."' || !e.isSourceLoaded) return;
		 
					map_". str_replace("-", "_", $mapID).".on('move', updateMarkers);
					map_". str_replace("-", "_", $mapID).".on('moveend', updateMarkers);
					updateMarkers();
				});
				";

				if(is_array($routes) && !empty($routes)){
					foreach($routes as $route){
					  if($route['routeProfile'] != "no"  && trim($route['routeGPX']) == "" ) {

						$scopScript .= " 
						var plotRoute;
						var xhttp = new XMLHttpRequest();
						xhttp.onreadystatechange = function() {
							if (this.readyState == 4 && this.status == 200) {
							var resp = JSON.parse(this.response);
							randNo = Math.ceil(Math.random() * Math.random() * Math.random() * 100);
							plotRoute = resp.routes[0].geometry;
							map_". str_replace("-", "_", $mapID).".addSource('route'+randNo, {
								'type': 'geojson',
								'data': {
									'type': 'Feature',
									'properties': {},
									'geometry': plotRoute
								}
							});
							map_". str_replace("-", "_", $mapID).".addLayer({
								'id': 'route'+randNo,
								'type': 'line',
								'source': 'route'+randNo,
								'layout': {
									'line-join': 'round',
									'line-cap': 'round'
								},
								'paint': {
									'line-color':     '". $route['routeColor'] ."',
									'line-width':     ". $route['routeLineWidth'] .",
									'line-opacity':   ". $route['routeLineOpacity'] .",
									'line-dasharray': [". $route['routeLineDash'] .",". $route['routeLineGap'] ."]
								}
							});
							
							}
						}
						xhttp.open('GET', 'https://api.mapbox.com/directions/v5/mapbox/". $route['routeProfile'] ."/". $route['routeCoords'] ."?geometries=geojson&access_token=". get_option('treweler_mapbox_access_token') ."', true);
						xhttp.send();
						";

					  } else {
						if(trim($route['routeGPX'])=="") {
							$fs_coords = [];
							$coords_str = explode(";", $route['routeCoords']);
							foreach($coords_str as $lls) {
								$ll = explode(",", $lls);
								$llA = [];
								foreach($ll as $l) {
									$llA[] = floatval($l);
								}
								$fs_coords[] = $llA;
							}

							$scopScript .= " 
							  var plotRoute = ". json_encode($fs_coords) .";
							  randNo = Math.ceil(Math.random() * Math.random() * 100);
							  map_". str_replace("-", "_", $mapID).".addSource('route'+randNo, {
								'type': 'geojson',
								'data': {
									'type': 'Feature',
									'properties': {},
									'geometry': {
										'type': 'LineString',
										'coordinates': plotRoute
									}
								}
							  });
							  map_". str_replace("-", "_", $mapID).".addLayer({
								'id': 'route'+randNo,
								'type': 'line',
								'source': 'route'+randNo,
								'layout': {
									'line-join': 'round',
									'line-cap': 'round'
								},
								'paint': {
									'line-color':     '". $route['routeColor'] ."',
									'line-width':     ". $route['routeLineWidth'] .",
									'line-opacity':   ". $route['routeLineOpacity'] .",
									'line-dasharray': [". $route['routeLineDash'] .",". $route['routeLineGap'] ."]
								}
							  });
							";
						} else {
							$scopScript .= "
							jQuery.ajax({
								type: 'GET',
								url: '". $route['routeGPX'] ."',
								dataType: 'xml',
								success: function(GPX){
									var points = [];
									jQuery(GPX).find('trkpt').each(function () {
										points.push([parseFloat(jQuery(this).attr('lon')), parseFloat(jQuery(this).attr('lat'))]);
									});
									if(points.length) {
										  randNo = Math.ceil(Math.random() * Math.random() * 100);
										  map_". str_replace("-", "_", $mapID).".addSource('route'+randNo, {
											'type': 'geojson',
											'data': {
												'type': 'Feature',
												'properties': {},
												'geometry': {
													'type': 'LineString',
													'coordinates': points
												}
											}
										  });
										  map_". str_replace("-", "_", $mapID).".addLayer({
											'id': 'route'+randNo,
											'type': 'line',
											'source': 'route'+randNo,
											'layout': {
												'line-join': 'round',
												'line-cap': 'round'
											},
											'paint': {
												'line-color':     '". $route['routeColor'] ."',
												'line-width':     ". $route['routeLineWidth'] .",
												'line-opacity':   ". $route['routeLineOpacity'] .",
												'line-dasharray': [". $route['routeLineDash'] .",". $route['routeLineGap'] ."]
											}
										  });
										
									}
								}
							})
						";
						}
					  }
					}
				}
			$scopScript .= " 
			});
			map_". str_replace("-", "_", $mapID).".on('mouseenter', 'clusters_". str_replace("-", "_", $mapID)."', function () {
				map.getCanvas().style.cursor = 'pointer';
			});
			map_". str_replace("-", "_", $mapID).".on('mouseleave', 'clusters_". str_replace("-", "_", $mapID)."', function () {
				map.getCanvas().style.cursor = '';
			});			
			";

			wp_add_inline_script('treweler-mapbox-geocoder-script', $scopScript);

			return $scop;
		}
		return;
	}
	return;
  }

  public function treweler_plugin_shortcode_add() {
    add_shortcode( TREWELER_PLUGIN_SHORTCODE_TAG, array($this, 'treweler_plugin_shortcode') );
  }

  public function treweler_remove_table_control(){
  ?><script>
	if( jQuery("#bulk-action-selector-top").length == 0 ){jQuery(".tablenav").css("display", "none");}
  </script><?php
  }

  /**
   * Save custom colors added by user - Colorpicker
   */
  function treweler_add_colorpicker_custom_color() {
	$color = trim($_POST['cust_color']);
	update_option( 'treweler_mapbox_colorpicker_custom_color', $color);
	echo get_option('treweler_mapbox_colorpicker_custom_color'); exit;
  }

}

new treweler();

endif;
