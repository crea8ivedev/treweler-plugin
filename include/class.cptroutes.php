<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! class_exists('cptMapRoutes') ) :

class cptMapRoutes {
  private static $initiated = false;

  function __construct() {
    if(! self::$initiated) {
      self::treweler_cpt_routes_init();
    }
  }

  function treweler_cpt_routes_init() {
    add_action( 'init', array($this, 'treweler_cpt_routes') );
    add_action( 'edit_form_after_title', array($this, 'add_placeholder_map_for_route') );

	add_action( 'add_meta_boxes', array($this, 'treweler_add_route_meta_init') ); //admin_init
    add_action( 'save_post_'. TREWELER_PLUGIN_CPT_ROUTE, array($this, 'treweler_save_route_meta_details'), 10, 3 );

	add_filter( 'manage_'. TREWELER_PLUGIN_CPT_ROUTE .'_posts_columns' , array($this, 'treweler_route_post_type_columns') );
	add_action( 'manage_'. TREWELER_PLUGIN_CPT_ROUTE .'_posts_custom_column' , array($this, 'treweler_route_fill_map_id_column'), 10, 2 );

	add_filter( 'upload_dir', array($this, 'treweler_map_route_file_upload_dir') );

  }

  /**
   * Treweler map - 'route' custom post type
   */
  function treweler_cpt_routes() {
    $labels = array(
      'name'                  => _x( 'Routes', 'Post type general name', TREWELER_TEXT_DOMAIN ),
      'singular_name'         => _x( 'Route', 'Post type singular name', TREWELER_TEXT_DOMAIN ),
      'add_new'               => __( 'Add New', TREWELER_TEXT_DOMAIN ),
      'add_new_item'          => __( 'Add New Route', TREWELER_TEXT_DOMAIN ),
      'edit_item'             => __( 'Edit Route', TREWELER_TEXT_DOMAIN ),
      'new_item'              => __( 'New Route', TREWELER_TEXT_DOMAIN ),
      'view_item'             => __( 'View Route', TREWELER_TEXT_DOMAIN ),
      'all_items'             => __( 'Routes', TREWELER_TEXT_DOMAIN ),
      'search_items'          => __( 'Search Routes', TREWELER_TEXT_DOMAIN ),
      'not_found'             => __( 'No Routes found.', TREWELER_TEXT_DOMAIN ),
      'not_found_in_trash'    => __( 'No Routes found in Trash.', TREWELER_TEXT_DOMAIN ),
      'menu_name'             => _x( 'Treweler', 'route', TREWELER_TEXT_DOMAIN ),
      'name_admin_bar'        => _x( 'Route', 'Add New on Toolbar', TREWELER_TEXT_DOMAIN ),
      'parent_item_colon'     => __( 'Parent Routes:', TREWELER_TEXT_DOMAIN ),

      'featured_image'        => _x( 'Import a GPS File', TREWELER_TEXT_DOMAIN ),
      'set_featured_image'    => _x( 'Upload file here', TREWELER_TEXT_DOMAIN ),
      'remove_featured_image' => _x( 'Remove this file', TREWELER_TEXT_DOMAIN ),
	  'use_featured_image'    => _x( 'Use as route', TREWELER_TEXT_DOMAIN ),

      'archives'              => _x( 'Route archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'insert_into_item'      => _x( 'Insert into route', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'uploaded_to_this_item' => _x( 'Uploaded to this route', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'filter_items_list'     => _x( 'Filter Routes list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'items_list_navigation' => _x( 'Routes list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
      'items_list'            => _x( 'Routes list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', TREWELER_TEXT_DOMAIN ),
    );

    $args = array(
      'labels'              => $labels,
      'description'         => 'Describe travels, routes, locations and other interesting events that can be described using maps.',
      'public'              => false,
      'publicly_queryable'  => false,
      'query_var'           => true,
      'show_ui'             => true,
      'show_in_menu'        => 'edit.php?post_type=map',
      'show_in_nav_menus'   => true,
      'capability_type'     => 'post',
      'has_archive'         => false,
      'hierarchical'        => false,
      'can_export'          => true,
      'menu_position'       => null,
      'rewrite'             => array( 'slug' => null, 'with_front' => false),
      'exclude_from_search' => false,
      'supports'            => array( 'title', 'page-attributes', 'thumbnail' ), //'custom-fields',
    );
    register_post_type( 'route', $args );
  }

  /**
   * Treweler map container block - route edit
   */
  function add_placeholder_map_for_route() {
    $post_type = get_post_type();
    if( $post_type == TREWELER_PLUGIN_CPT_ROUTE ) {
		$routeProfile = get_post_meta(get_the_ID(), "_treweler_route_profile", true);
		$routeProfile = ($routeProfile!="" ? $routeProfile : "no");
      echo '
		  <div class="mapbox-directions-profile">
			<input id="mapbox-directions-profile-no" type="radio" name="profile" value="no" '. ($routeProfile=="no"?"checked":"") .'>
			<label for="mapbox-directions-profile-no">No Match</label>
			<input id="mapbox-directions-profile-driving" type="radio" name="profile" value="driving" '. ($routeProfile=="driving"?"checked":"") .'>
			<label for="mapbox-directions-profile-driving">Driving</label>
			<input id="mapbox-directions-profile-walking" type="radio" name="profile" value="walking" '. ($routeProfile=="walking"?"checked":"") .'>
			<label for="mapbox-directions-profile-walking">Walking</label>
			<input id="mapbox-directions-profile-cycling" type="radio" name="profile" value="cycling" '. ($routeProfile=="cycling"?"checked":"") .'>
			<label for="mapbox-directions-profile-cycling">Cycling</label>
		  </div>
		  
		  <div id="route_map" class="treweler-mapbox"></div>
		  
		  <!-- Create a container for the instructions and directions -->
		  <div class="info-box">
		    <div id="info">
		  	  <p>Draw your route using the draw tools in the upper right corner of the map. To get the most accurate route match, draw points at regular intervals.</p>
		    </div>
		    <div id="directions"></div>
		  </div>
	  ';
    }
  }

  /**
   * Add custom column in post type - `route` table
   */
  function treweler_route_post_type_columns($columns){
	return array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Title'),
		'map_title' =>__( 'Route Map'),
		'date' =>__( 'Date')
	);
  }

  function treweler_route_fill_map_id_column($column, $post_id){
	switch ( $column ) {
		case 'map_title' :
			if(get_post_meta($post_id, '_treweler_route_map_id', true) != ""){
			  echo '<a href="'. admin_url() .'post.php?post='. get_post_meta($post_id, '_treweler_route_map_id', true) .'&action=edit">'. get_the_title( get_post_meta($post_id, '_treweler_route_map_id', true) ) .'</a>';
			}
			break;
	}
  }

  /**
   * Route custom meta fields
   */
  function treweler_add_route_meta_init(){
    add_meta_box("treweler_map_route_settings_controls-meta", esc_html__( "Route Settings", TREWELER_TEXT_DOMAIN ), array($this, "treweler_map_route_settings_controls"), "route", "side", "low");
  }

  function treweler_map_route_settings_controls(){
    require TREWELER_PLUGIN_DIR . 'templates/admin/route-control-settings-template.php';
  }

  function treweler_save_route_meta_details($post_id, $post, $update){

	if( !empty($_POST) && TREWELER_PLUGIN_CPT_ROUTE == $post->post_type ) {

		if(isset($_POST["profile"]))
			update_post_meta($post->ID, "_treweler_route_profile", sanitize_text_field($_POST["profile"]));
		if(isset($_POST["map_id"]))
			update_post_meta($post->ID, "_treweler_route_map_id", sanitize_text_field($_POST["map_id"]));
		if(isset($_POST["route_line_width"]))
			update_post_meta($post->ID, "_treweler_route_line_width", sanitize_text_field($_POST["route_line_width"]));
		if(isset($_POST["route_line_opacity"]))
			update_post_meta($post->ID, "_treweler_route_line_opacity", $_POST["route_line_opacity"]);
		if(isset($_POST["route_line_dash"]))
			update_post_meta($post->ID, "_treweler_route_line_dash", $_POST["route_line_dash"]);
		if(isset($_POST["route_line_gap"]))
			update_post_meta($post->ID, "_treweler_route_line_gap", $_POST["route_line_gap"]);
		if(isset($_POST["routeColor"]))
			update_post_meta($post->ID, "_treweler_route_line_color", $_POST["routeColor"]);
		if(isset($_POST["setZoom"]))
			update_post_meta($post->ID, "_treweler_route_map_zoom", $_POST["setZoom"]);
		if(isset($_POST["latlng"]))
			update_post_meta($post->ID, "_treweler_route_map_latlng", sanitize_text_field($_POST["latlng"]));
		if(isset($_POST["routeCoords"]))
			update_post_meta($post->ID, "_treweler_route_line_coords", sanitize_text_field($_POST["routeCoords"]));
		if(isset($_POST["routeGPXFile"]))
			update_post_meta($post->ID, "_treweler_route_gpx_file", sanitize_text_field($_POST["routeGPXFile"]));

	} else {
		return;
	}

  }

  /**
   * Add new folder for routes file in uploads folder
   */
  function treweler_map_route_file_upload_dir( $args ) {
	$id = ( isset( $_REQUEST['post_id'] ) ? $_REQUEST['post_id'] : '' );
	if( $id ) {
		if(get_post_type( $id ) == TREWELER_PLUGIN_CPT_ROUTE){
		  // Set the new path for 'route' files
	      $newdir = '/' . get_post_type( $id );

	      $args['path']    = str_replace( $args['subdir'], '', $args['path'] ); //remove default subdir
	      $args['url']     = str_replace( $args['subdir'], '', $args['url'] );
	      $args['subdir']  = $newdir;
	      $args['path']   .= $newdir;
	      $args['url']    .= $newdir;
		}
	}
	return $args;
  }

}

endif;
