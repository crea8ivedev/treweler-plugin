<?php
/**
 * Plugin Name: Treweler
 * Description: Describe travels, routes, locations and other interesting events that can be described using maps.
 * Version: 0.22
 * Author: Aisconverse
 * Plugin URI: http://treweler.aisconverse.com/
 * Author URI: http://aisconverse.com/
 * License: GPLv3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: treweler
 * Domain Path: /languages/
 *
 * @package Treweler
 */

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Basic plugin definations
 *
 * @package Treweler
 * @since 0.1
 *
 **/
if( !defined( 'TREWELER_VERSION' ) ) {
  define( 'TREWELER_VERSION', '0.22' );  //Plugin version number
}

if( !defined( 'TREWELER_MINIMUM_WP_VERSION' ) ) {
  define( 'TREWELER_MINIMUM_WP_VERSION', '4.0' );  //Supported wordpress version number
}

if( !defined( 'TREWELER_PLUGIN_DIR' ) ) {
  define( 'TREWELER_PLUGIN_DIR', plugin_dir_path( __FILE__ ) ); //Plugin directory path
}

if( !defined( 'TREWELER_PLUGIN_URL' ) ) {
  define( 'TREWELER_PLUGIN_URL', plugin_dir_url( __FILE__ ) ); //Plugin path
}

if( !defined( 'TREWELER_TEXT_DOMAIN' ) ) {
  define( 'TREWELER_TEXT_DOMAIN', 'treweler' ); //Plugin text domain constant
}

if( !defined( 'TREWELER_PLUGIN_CPT_MAP' ) ) {
  define( 'TREWELER_PLUGIN_CPT_MAP', 'map' ); //Plugin custom post type name `map`
}

if( !defined( 'TREWELER_PLUGIN_CPT_MARKER' ) ) {
  define( 'TREWELER_PLUGIN_CPT_MARKER', 'marker' ); //Plugin custom post type name `marker`
}

if( !defined( 'TREWELER_PLUGIN_CPT_ROUTE' ) ) {
  define( 'TREWELER_PLUGIN_CPT_ROUTE', 'route' ); //Plugin custom post type name `route`
}

if( !defined( 'TREWELER_PLUGIN_SHORTCODE_TAG' ) ) {
  define( 'TREWELER_PLUGIN_SHORTCODE_TAG', 'treweler' ); //Shortcode tag `treweler`
}

/**
 * Activation Hook
 *
 * Register plugin activation hook.
 *
 * @package Treweler
 * @since 0.1
 */
register_activation_hook( __FILE__, array( 'Treweler', 'plugin_activation' ) );

/**
 * Deactivation Hook
 *
 * Register plugin activation hook.
 *
 * @package Treweler
 * @since 0.1
 */
register_deactivation_hook( __FILE__, array( 'Treweler', 'plugin_deactivation' ) );

/**
 * Include the Treweler class for init
 */
require_once( TREWELER_PLUGIN_DIR . 'include/class.treweler.php' );

